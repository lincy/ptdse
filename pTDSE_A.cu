#include "pTDSE.h"

#include <cuda_runtime.h>
#include "checkcuda.h"
#include <cassert>

__global__
void prepareA_kernel(real*_RESTRICT_ Ar, real*_RESTRICT_ Ai, 
					 const real*_RESTRICT_ psi, const real*_RESTRICT_ Eln, const real*_RESTRICT_ dF, 
					 const real hdt, const int pDIM, const int nDIM, const int Lmax)   {
		
	int l = blockIdx.x;
	int j = blockIdx.y;
	int k = threadIdx.x;

	if (l>Lmax || j>=pDIM || k>=pDIM)  return; 
	
	int idx = IA(l, j, k);
	Ar[idx] = 0; Ai[idx] = 0;
	for (int n = 0; n < nDIM; n++) {
		real tmp = Psi(l,n,j)*Psi(l,n,k) * dF[k];
		Ar[idx] += tmp*cos( hdt * Eln(l,n) );
		Ai[idx] -= tmp*sin( hdt * Eln(l,n) );
	}
}

//
//   Each block handle a whole lm-mode. Set BlockDim = pDIM.
//
void pTDSE::gPrepareA() {

	assert(pDIM<=1024);   // Can be released later for futher evaluation.

    real *gPsi, *gEln;
 	checkcuda(cudaMalloc((void**)&gPsi, ((Lmax+1)*nDIM*pDIM)*sizeof(real)));
 	checkcuda(cudaMalloc((void**)&gEln, ((Lmax+1)*nDIM)     *sizeof(real)));
	checkcuda(cudaMemcpy(gPsi, psi, (size_t)((Lmax+1)*nDIM*pDIM)*sizeof(real), cudaMemcpyHostToDevice));
	checkcuda(cudaMemcpy(gEln, Eln, (size_t)((Lmax+1)*nDIM)     *sizeof(real), cudaMemcpyHostToDevice));
    
	dim3 dimGrid(Lmax+1,pDIM);  // l,k
	dim3 blkGrid(pDIM, 1);
	prepareA_kernel<<<dimGrid, blkGrid>>>(g_Ar, g_Ai, gPsi, gEln, g_dF, 0.5*dt, pDIM, nDIM, Lmax);
    cudaDeviceSynchronize();

	cudaFree(gPsi); 
	cudaFree(gEln); 
}

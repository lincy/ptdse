#ifndef GLOBAL_HH
#define GLOBAL_HH

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>

#include <iostream>
#include <fstream>
using std::ifstream;
using std::endl;
using std::cout;
using std::min;
using std::max;

#undef SINGLE_PRECISION    // Not implemented for function of generating wigner matrix
#ifdef SINGLE_PRECISION
typedef float real;
#else
typedef double real;
#endif

template<typename T>
T min(T* arr, const int n, int& minidx) {
	T minval = arr[0];
	//#pragma omp parallel for reduction(min : minval)
	for(int i=0;i<n;i++) { if (arr[i]<minval) { minidx=i; minval=arr[i]; } }
	return minval;
}

template<typename T>
T max(T* arr, const int n, int& maxidx) {
	T maxval = arr[0];
	//#pragma omp parallel for reduction(max : maxval)
	for(int i=0;i<n;i++) { if (arr[i]>maxval) { maxidx=i; maxval=arr[i]; } }
	return maxval;
}


#define FC(x) x##_
extern "C" {
	void dsteqr_(char*, int&, real*, real*, real*, int&, real*, int&);
	void ssteqr_(char*, int&, real*, real*, real*, int&, real*, int&);
	//
	//  djmn = dj(-n)(-m)
	//  djmn(-th) = djmn(th) * (-1)**(m-n)
	real djmn_(int&j, int&m, int&n, real&rad);
	real plgndr_(int&l,int&m,real&rad);   // F90 version of Plm, only for test
}


void nPlm_value(double tet, int L, double* Plm);
void Plm_value(double tet, int L, double* Plm);
real factorial(int i);

#endif

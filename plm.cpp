#include <cmath>


void nPlm_value(double tet, int L, double* Plm) {
    double ct = cos(tet), st = sin(tet);
    *(Plm++) = 1.0/sqrt(2), *(Plm++) = ct*sqrt(1.5), *(Plm++) = -st*sqrt(0.75); //P00,10,11
    double dfac = 1, f2l = 2;
    for(int l=2;l<=L;l++) {
    	int l2m1 = 2*l-1; int l2p1 = 2*l+1; int l2m3 = 2*l-3;
    	double sl2p1 = sqrt(double(l2p1));
    	for(int m=0;m<l-1;m++) {  // m=0...l-2
    		double a = *(Plm-l), b = *(Plm-l2m1);
    		double cA = sqrt( double(l2m1           )/(     (l-m)*(l+m)) );
    		double cB = sqrt( double((l+m-1)*(l-m-1))/(l2m3*(l-m)*(l+m)) );
    		*(Plm++) = sl2p1 * ( cA*ct*a - cB*b );  // P_l_m ~ P_l-1_m + P_l-2_m
        }
        double Pll = *(Plm-l);
        double tmp = ct*sl2p1*Pll;
        *(Plm++) = tmp;  // P_l_l+1 ~ P_ll       // m=l-1
        dfac *= l2m1, f2l *= 2*l*l2m1;
        *(Plm++) = pow(-1,l)*dfac*pow(st,l)*sqrt(0.5*l2p1/f2l);   // m=l
    }
}

//  "Un-normalized Legender polynominal - Plm"
//  P00, 10 11,  20 21 22, 30 31 32 33, ...
//	for m>=0
//
void Plm_value(double tet, int L, double* Plm) {
    double ct = cos(tet), st = sin(tet);
    *(Plm++) = 1.0, *(Plm++) = ct, *(Plm++) = -st; //P00,10,11
    int dfac = 1; // initially 1!!
    for(int l=2;l<=L;l++) {
    	int l2m1 = 2*l-1;
    	for(int m=0;m<l-1;m++) {  // m=0...l-2
    		double a = *(Plm-l), b = *(Plm-l2m1);
    		*(Plm++) = ( l2m1*ct*a - (l+m-1)*b ) / (l-m);  // P_l_m ~ P_l-1_m + P_l-2_m
        }
        double Pll = *(Plm-l);
        double tmp = ct*l2m1*Pll;
        *(Plm++) = tmp;  // P_l_l+1 ~ P_ll
        //*(Plm++) = ist * ( ct*tmp - l2m1*Pll);   // m=l, fall when st=0
        dfac *= l2m1;
        *(Plm++) = pow(-1,l)*dfac*pow(st,l);   // m=l
    }
}

double factorial(int f)
{
    if ( f == 0 ) return 1.0;

    double fac = 1.0;
    for(int i=2; i<f; i++) fac *= i;
    return(f * fac);
}


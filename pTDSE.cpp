#include "pTDSE.h"
#include "checkcuda.h"
#include <vector>

const real ONE=1.0, ZEO=0.0, MONE=-1.0;

//
//  For elliptic polarization:
//
void pTDSE::Evolve() {
	//=====  Monitor parameter : should edit here only =============
	const int  NORMCHECK_INTERVAL = 100;
	const int   ANALYSIS_INTERVAL = 1000;
	const bool   INIT_ANAL = false;
	const bool MIDDLE_ANAL = false;
	const bool  FINAL_ANAL = true;
	const bool OUTPUT_PAD = true;
	const bool DUMP_STATE = true;   // to Starr
	const bool DUMP_BINARY = false;
	//int IT_MAX = laserT / dt;
	int IT_MAX = 1000; //laserT / dt;
	#define DEBUGSTARR 0
	//==================================================================

	char monitor[256];
	if ( INIT_ANAL ) {
		sprintf(monitor, "%s/StateI.dat", getOutPrefix());
		outputState(monitor, f0r, f0i);
		if ( OUTPUT_PAD ) 	{
			sprintf(monitor, "%s/PAP.bin", getOutPrefix());
			outputPAD(monitor, f0r, f0i);
			calculateATI(f0r, f0i, f1r, f1i);
			sprintf(monitor, "%s/PAD.bin", getOutPrefix());
			outputPAD(monitor, f1r, f1i);
		}
		if ( DUMP_BINARY ) {
			sprintf(monitor, "%s/Dump.bin", getOutPrefix());
			DumpBinary(monitor, f0r, f0i);
		}
	}
	{
	    printf("= Initial Wigner Rotation.\n");
#ifdef GPU_RUN
		prepareWignerD(valWd1, CEP );
		gRotateState(valWd1, g_f0r, g_f0i, g_f1r, g_f1i);
		swapState(&g_f0r,&g_f0i, &g_f1r,&g_f1i);
		prepareWignerD(valWd1,  -0.5*omega*getDt());

		//prepareWignerD(valWd2, -0.5*getOmega()*getDt());
		//Wav2Host();
		//sprintf(monitor, "%s/StateIR.dat", getOutPrefix());
		//outputState(monitor, f0r, f0i);
#else
		calWignerD(Wd1, CEP );
		RotateState(Wd1, f0r, f0i, f1r, f1i);
		swapState(&f0r,&f0i,&f1r,&f1i);
		calWignerD(Wd1,  -0.5*omega*dt);
#endif
	}

	// prepare monitor A
 	FILE * polar_monitor;
	sprintf(monitor, "%s/Tracker.dat", getOutPrefix());
	polar_monitor = fopen(monitor, "w");

	// ================================================================
	printf("Iterations: %d\n", IT_MAX);
	for (int it=0; it <= IT_MAX; it++) {
		
		phy_time = dt*(it + 1);
		real mid_time = dt*(it+0.5);
		//At = pow(sin(M_PI*mid_time/laserT), 2) *Em*dt / omega / sqrt(2.0);
		
		real ft = pow(sin(M_PI*mid_time/laserT), 2);
		real Ex = cos(omega*mid_time+CEP)*cos(0.5*epsilon);
		real Ey = sin(omega*mid_time+CEP)*sin(0.5*epsilon);
		At = ft * Em*dt/omega * sqrt(Ex*Ex+Ey*Ey);
		
		fprintf (polar_monitor, "%g %g\n", ft*Ex, ft*Ey);

		// Et NOT used.
		//real Et =   Em* pow(sin(phy_time*M_PI/laserT), 2) * cos(omega*phy_time+CEP)
        //		  + Em* M_PI/(omega*laserT)*sin(2*M_PI*phy_time/laserT)*sin(omega*phy_time+CEP);

		if (it%NORMCHECK_INTERVAL==0) {
			main_timer->Stop();
#ifdef GPU_RUN
			printf("--- it %5d : tf= %9g Norm= %10.8g  At=%16.10g -- %.2f secs\n", it, phy_time, gCheckNorm(g_f0r, g_f0i, g_f1r, g_f1i), At/dt, main_timer->wtime() );
#else
			printf("--- it %5d : tf= %9g Norm= %10.8g  At=%16.10g -- %.2f secs\n", it, phy_time, checkNorm(f0r, f0i), At/dt, main_timer->wtime() );
#endif
			main_timer->Reset();
			main_timer->Start();

#if DEBUGSTARR
			Wav2Host();
			printf("%12g %12g %12g %12g %12g %12g %12g %12g %12g %12g %12g %12g\n",
					f0r[IF(0,0,511)], f0i[IF(0,0,511)] , f0r[IF(1,0,511)], f0i[IF(1,0,511)], f0r[IF(1,1,511)], f0i[IF(1,1,511)],
					f0r[IF(1,2,511)], f0i[IF(1,2,511)] , f0r[IF(2,0,511)], f0i[IF(2,0,511)], f0r[IF(2,1,511)], f0i[IF(2,1,511)]
			);
#endif
		}
#ifdef GPU_RUN
		//
		//  CAUTION: EvolveH0 decrease norm linearly and slightly, 1e-8 for one step, Is it due to single precision initial data?
		//
		gEvolveH0(g_f0r, g_f0i, g_f1r, g_f1i);
		
		gRotateState(valWd1, g_f1r, g_f1i, g_f0r, g_f0i);  //== Wigner rotate by 0.5*dt*omega

		//swapState(&g_f0r,&g_f0i, &g_f1r,&g_f1i);
		gEvolveInt(g_f0r, g_f0i, g_f1r, g_f1i);

		gRotateState(valWd1, g_f1r, g_f1i, g_f0r, g_f0i);
		
		gEvolveH0(g_f0r, g_f0i, g_f1r, g_f1i);
		swapState(&g_f0r,&g_f0i, &g_f1r,&g_f1i);
#else
		EvolveH0(f0r, f0i, f1r, f1i);
		RotateState(Wd1, f1r, f1i, f0r, f0i);
		EvolveInteraction(f0r, f0i, f1r, f1i);
		RotateState(Wd1, f1r, f1i, f0r, f0i);
		EvolveH0(f0r, f0i, f1r, f1i);
		swapState(&f0r,&f0i,&f1r,&f1i);
#endif
		// =======> Intermedia Analysis
		if ( MIDDLE_ANAL && it%ANALYSIS_INTERVAL==0 ) {
			#ifdef GPU_RUN
			Wav2Host();
			#endif

			#ifndef NO_ANA_STATE
			// output wavefunction
			if ( DUMP_STATE ) {
				sprintf(monitor, "%s/State%04d.dat", getOutPrefix(), it);
				outputState(monitor, f0r, f0i);
				sprintf(monitor, "%s/Dump%04d.dat", getOutPrefix(), it);
				DumpState(monitor, f0r, f0i);
			} else if ( DUMP_BINARY ) {
				sprintf(monitor, "%s/Dump%04d.bin", getOutPrefix(), it);
				DumpBinary(monitor, f0r, f0i);
			}
			#endif

			#ifndef ANA_STATE
			if ( OUTPUT_PAD )  {
				sprintf(monitor, "%s/PAP%04d.bin", getOutPrefix(), it);
				outputPAD(monitor, f0r, f0i);

				calculateATI(f0r, f0i, f1r, f1i);
				sprintf(monitor, "%s/ATI.dat", getOutPrefix());
				outputATI(monitor);

				sprintf(monitor, "%s/PAD%04d.bin", getOutPrefix(), it);
				outputPAD(monitor, f1r, f1i);
			}
			#endif
		}
	}
	
	fclose(polar_monitor);

#if DEBUGSTARR
			Wav2Host();
			printf("%12g %12g %12g %12g %12g %12g %12g %12g %12g %12g %12g %12g\n",
					f0r[IF(0,0,511)], f0i[IF(0,0,511)] , f0r[IF(1,0,511)], f0i[IF(1,0,511)], f0r[IF(1,1,511)], f0i[IF(1,1,511)],
					f0r[IF(1,2,511)], f0i[IF(1,2,511)] , f0r[IF(2,0,511)], f0i[IF(2,0,511)], f0r[IF(2,1,511)], f0i[IF(2,1,511)]
			);
#endif
	
	// Wigner rotated back
	{
		printf("= Final Wigner Rotation.\n");
#ifdef GPU_RUN
		prepareWignerD(valWd1,  omega*phy_time + CEP );
		gRotateState(valWd1, g_f0r, g_f0i, g_f1r, g_f1i);
		swapState(&g_f0r,&g_f0i, &g_f1r,&g_f1i);
#else
		calWignerD(Wd1,  omega*phy_time - CEP );
		RotateState(Wd1, f0r, f0i, f1r, f1i);
		swapState(&f0r,&f0i,&f1r,&f1i);
#endif
	}
#if DEBUGSTARR
			Wav2Host();
			printf("%12g %12g %12g %12g %12g %12g %12g %12g %12g %12g %12g %12g\n",
					f0r[IF(0,0,511)], f0i[IF(0,0,511)] , f0r[IF(1,0,511)], f0i[IF(1,0,511)], f0r[IF(1,1,511)], f0i[IF(1,1,511)],
					f0r[IF(1,2,511)], f0i[IF(1,2,511)] , f0r[IF(2,0,511)], f0i[IF(2,0,511)], f0r[IF(2,1,511)], f0i[IF(2,1,511)]
			);
#endif

	if (FINAL_ANAL) {
		#ifdef GPU_RUN
		Wav2Host();
		#endif
		// output final state
		sprintf(monitor, "%s/StateF.dat", getOutPrefix() );
		outputState(monitor, f0r, f0i);
		if ( OUTPUT_PAD ) {
			sprintf(monitor, "%s/PAP_final.bin", getOutPrefix());
			outputPAD(monitor, f0r, f0i);
			calculateATI(f0r, f0i, f1r, f1i);
			sprintf(monitor, "%s/ATI.dat", getOutPrefix());
			outputATI(monitor);
			sprintf(monitor, "%s/PAD_final.bin", getOutPrefix());
			outputPAD(monitor, f1r, f1i);
		}
		if ( DUMP_STATE ) {
			sprintf(monitor, "%s/DumpF.dat", getOutPrefix());
			DumpState(monitor, f0r, f0i);
		} else if ( DUMP_BINARY ) {
			sprintf(monitor, "%s/DumpF.bin", getOutPrefix());
			DumpBinary(monitor, f0r, f0i);
		}

	}
}

void pTDSE::initF() {

	cout << "== Initializing Psi ..." << endl;

	memset (f0r, 0, (Lmax+1)*(Lmax+1)*pDIM);
	memset (f0i, 0, (Lmax+1)*(Lmax+1)*pDIM);
	for (int j = 0; j < pDIM;  j++) {

		// ========= Set initial state
		// Note that the numeical value in wav.dat is \psi*p (physical value multiplied by p)
		// so, \Psi(p) = f_{lm} / p * Y_{lm}, on the node

		f0r[IF(0,0,j)] = -Psi(0,0,j);    // 1s state : == \psi_{nl=10} * p
		
		//f0r[IF(1,1,j)] = Psi(1,1,j);    // initial state n,l,m = 2,1, 0
		//f0r[IF(1,0,j)] = Psi(1,1,j);    // initial state n,l,m = 2,1,-1   // plotDump fail!!??
		//f0r[IF(1,2,j)] = Psi(1,1,j);    // initial state n,l,m = 2,1, 1   // plotDump fail!!??
	}


#ifdef GPU_RUN

		checkcuda(cudaMemcpy(g_f0r, f0r, (size_t)((Lmax+1)*(Lmax+1)*pDIM)*sizeof(real), cudaMemcpyHostToDevice));
		checkcuda(cudaMemcpy(g_f0i, f0i, (size_t)((Lmax+1)*(Lmax+1)*pDIM)*sizeof(real), cudaMemcpyHostToDevice));

		// Init Wigner Rotation first for avoiding ran out of memory
		cout << "== Prepare sparse Wigner D..." << endl;
		{  // alllocate sparse WigberD
		const int nnz = pDIM * ( IWd(Lmax+1,0,0)   - 1 );
		const int n   = pDIM * ( (Lmax+1)*(Lmax+1) - 1 );
		checkcuda(cudaMalloc((void**)&cooColWd, nnz*sizeof(int)));
		checkcuda(cudaMalloc((void**)&csrRowWd, (n+1)*sizeof(int)));
		checkcuda(cudaMalloc((void**)&valWd1,    nnz*sizeof(real)));
		//checkcuda(cudaMalloc((void**)&valWd2,    nnz*sizeof(real)));
		}
#endif
}

void pTDSE::Wav2Host() {
	//GPU
	checkcuda(cudaMemcpy(f0r, g_f0r, (size_t)((Lmax+1)*(Lmax+1)*pDIM)*sizeof(real), cudaMemcpyDeviceToHost));
	checkcuda(cudaMemcpy(f0i, g_f0i, (size_t)((Lmax+1)*(Lmax+1)*pDIM)*sizeof(real), cudaMemcpyDeviceToHost));
}

//
//  TODO: we need a GPU version. This is slow.
//
void pTDSE::calculateATI(real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i) {

	//
	// Calculate continuous state
	//
	#pragma omp parallel for
	for (int l = 0; l <= Lmax; l++) {   //printf("%d \n", l);
	for (int m = 0; m <= 2*l;  m++) {
	for (int j = 0; j < pDIM;  j++) {
		real sum_nr = 0;
		real sum_ni = 0;
		for (int n = 0; n < nbound[l]; n++) {
			real sum_kr = 0;
			real sum_ki = 0;
			for (int k = 0; k < pDIM; k++) {
				int lmk = IF(l,m,k);
				real tmp = Psi(l,n,k) * dF[k];
				sum_kr += in_r[lmk]*tmp;
				sum_ki += in_i[lmk]*tmp;
			}
			sum_nr += sum_kr * Psi(l,n,j);
			sum_ni += sum_ki * Psi(l,n,j);
		}
		int lmj = IF(l,m,j);
		out_r[lmj] = in_r[lmj] - sum_nr;
		out_i[lmj] = in_i[lmj] - sum_ni;

	}}}

	//
	//  Calculate ATI: (\pd P) / (\pd e). Note de = pdp
	//
 	#pragma omp parallel for
	for (int j = 0; j < pDIM;  j++) {
		ATI[j] = 0;
		for (int l = 0; l <= Lmax; l++)
		for (int m = 0; m <= 2*l;  m++) {
			int lmj = IF(l,m,j);
			ATI[j] += out_r[lmj]*out_r[lmj] + out_i[lmj]*out_i[lmj];
		}
		ATI[j] /= p[j];
	}
}

void pTDSE::EvolveH0(real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i) {
	//cout << "== Evolving H0 ..." << endl;
	#pragma omp parallel for
	for (int l = 0; l <= Lmax; l++)
	for (int m = 0; m <= 2*l;  m++)
	for (int j = 0; j < pDIM; j++) {
		int lmj = IF(l,m,j);
		out_r[lmj] = 0;
		out_i[lmj] = 0;
		for (int k=0; k<pDIM; k++) {
			int ljk = IA(l,j,k);
			int lmk = IF(l,m,k);
			out_r[lmj] += Ar[ljk] * in_r[lmk] - Ai[ljk] * in_i[lmk];
			out_i[lmj] += Ar[ljk] * in_i[lmk] + Ai[ljk] * in_r[lmk];
		}
	}
#if 0
	for (int l = 1; l <= 1; l++)
	for (int j = 0; j < pDIM; j++) {
		cout << Ai[IA(l,j,j)] << endl;
	}
#endif
}

void pTDSE::gEvolveH0(real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i) {
	
	const int N_streams = 2*(Lmax+1);   // each stream for a real/img part of l,m
	cudaStream_t *streams = (cudaStream_t *)malloc(N_streams*sizeof(cudaStream_t*));

	for(int l=0; l<=Lmax; l++) {
		checkcuda(cudaStreamCreate(&(streams[2*l])));
		checkcuda(cudaStreamCreate(&(streams[2*l+1])));
		
		int idx = IF(l,0,0);
		int ida = IA(l,0,0);
		// g_{lmj} = A_^(l)_{jk} f_{lmk}  ==>   g=f A^T
#ifdef SINGLE_PRECISION
		// CHECK LATER ~~~~~
		cublasSetStream(handblas,streams[2*l]);	
		cublasSgemm_v2(handblas,CUBLAS_OP_N,CUBLAS_OP_T, pDIM, 2*l+1, pDIM, &ONE, &in_r[idx],pDIM, g_Ar,pDIM, &ZEO, &out_r[idx], pDIM);
	    cublasSetStream(handblas,streams[2*l+1]);
		cublasSgemm_v2(handblas,CUBLAS_OP_N,CUBLAS_OP_T, pDIM, 2*l+1, pDIM, &ONE, &in_i[idx],pDIM, g_Ai,pDIM, &ZEO, &out_i[idx], pDIM);
#else
		//cublasSetStream(handblas,streams[2*l]);
		//cublasDgemm_v2(handblas,CUBLAS_OP_N,CUBLAS_OP_T, pDIM, 2*l+1, pDIM, &ONE, &in_r[idx],pDIM, g_Ar,pDIM, &ZEO, &out_r[idx], pDIM);
	    //cublasSetStream(handblas,streams[2*l+1]);
		//cublasDgemm_v2(handblas,CUBLAS_OP_N,CUBLAS_OP_T, pDIM, 2*l+1, pDIM, &ONE, &in_i[idx],pDIM, g_Ai,pDIM, &ZEO, &out_i[idx], pDIM);

		cublasSetStream(handblas,streams[2*l]);
		cublasDgemm_v2(handblas,CUBLAS_OP_T,CUBLAS_OP_N, pDIM, 2*l+1, pDIM, &ONE, &g_Ar[ida], pDIM, &in_r[idx], pDIM, &ZEO, &out_r[idx], pDIM);
		cublasDgemm_v2(handblas,CUBLAS_OP_T,CUBLAS_OP_N, pDIM, 2*l+1, pDIM, &MONE,&g_Ai[ida], pDIM, &in_i[idx], pDIM, &ONE, &out_r[idx], pDIM);
	    cublasSetStream(handblas,streams[2*l+1]);
		cublasDgemm_v2(handblas,CUBLAS_OP_T,CUBLAS_OP_N, pDIM, 2*l+1, pDIM, &ONE, &g_Ar[ida], pDIM, &in_i[idx], pDIM, &ZEO, &out_i[idx], pDIM);
		cublasDgemm_v2(handblas,CUBLAS_OP_T,CUBLAS_OP_N, pDIM, 2*l+1, pDIM, &ONE, &g_Ai[ida], pDIM, &in_r[idx], pDIM, &ONE, &out_i[idx], pDIM);

		//cudaDeviceSynchronize();
#endif
		}
	
	for(int l=0; l<=Lmax; l++) {
		checkcuda(cudaStreamDestroy(streams[2*l]));
		checkcuda(cudaStreamDestroy(streams[2*l+1]));
	}
}

void pTDSE::gRotateState(real*_RESTRICT_ R, real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i) {
		
	//chkCuSparse();
	cudaMemcpy(out_r, in_r, pDIM*sizeof(real), cudaMemcpyDeviceToDevice);   // l=0 do not transform
	cudaMemcpy(out_i, in_i, pDIM*sizeof(real), cudaMemcpyDeviceToDevice);
	
	const int nnz = pDIM * ( IWd(Lmax+1,0,0)   - 1 );
    const int n   = pDIM * ( (Lmax+1)*(Lmax+1) - 1 );
#ifdef SINGLE_PRECISION
	status = cusparseScsrmv(handWd, CUSPARSE_OPERATION_NON_TRANSPOSE, n, n, nnz, &ONE, gMatDescr, R, csrRowWd, cooColWd, &in_r[pDIM], &ZEO, &out_r[pDIM]);
	if (status != CUSPARSE_STATUS_SUCCESS) { printf("Matrix-vector multiplication failed-R\n"); return; }
    status = cusparseScsrmv(handWd, CUSPARSE_OPERATION_NON_TRANSPOSE, n, n, nnz, &ONE, gMatDescr, R, csrRowWd, cooColWd, &in_i[pDIM], &ZEO, &out_i[pDIM]);
	if (status != CUSPARSE_STATUS_SUCCESS) { printf("Matrix-vector multiplication failed-I\n"); return; }
#else
	status = cusparseDcsrmv(handWd, CUSPARSE_OPERATION_NON_TRANSPOSE, n, n, nnz, &ONE, gMatDescr, R, csrRowWd, cooColWd, &in_r[pDIM], &ZEO, &out_r[pDIM]);
	if (status != CUSPARSE_STATUS_SUCCESS) { printf("Matrix-vector multiplication failed-R\n"); return; }
    status = cusparseDcsrmv(handWd, CUSPARSE_OPERATION_NON_TRANSPOSE, n, n, nnz, &ONE, gMatDescr, R, csrRowWd, cooColWd, &in_i[pDIM], &ZEO, &out_i[pDIM]);
	if (status != CUSPARSE_STATUS_SUCCESS) { printf("Matrix-vector multiplication failed-I\n"); return; }
#endif
}

void pTDSE::EvolveInteraction(real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i) {
	//
	//  Lambda_{ll'} = \sum_k R_{kl} * cos(A_0 p_j delta E_k )    * R_{kl'}
	//
	#pragma omp parallel for
	for (int l = 0; l <= Lmax; l++)
	for (int m = 0; m <= 2*l;  m++) {

		int apm = std::abs(m - l);  // absolute value of physical m
		int KD = Lmax-apm+1;
		int la = l-apm;   // l in term of {m}-space

		for (int j = 0; j < pDIM; j++) {

			int lmj = IF(l,m,j);
			out_r[lmj] = 0;
			out_i[lmj] = 0;

			for (int lb = 0; lb < KD; lb++) {

				// <<<<=========================
				// This subloop calcuate Lambda^{jm}_{ab}, which is initialize as \delta_{ab}. And since the eigenvalue are in order as -2 -1 0 1 2 ..., so I sum it in group.
				//
				real Lr = 0, Li = 0;
#define SMART_SUM
#ifndef SMART_SUM
				// These too methods give almost the same digits
				printf("** No SMART_SUM **");
				for (int k = 0; k < KD; k++) {   // dim from [L+1,2]
					real angle = At * p[j] * lam[Il(apm,k)];
					real tmp   = R[IR(apm,k,la)] * R[IR(apm,k,lb)];
					//real tmp   = R[IR(apm,la,k)] * R[IR(apm,lb,k)];
					Lr += tmp * cos(angle);
					Li -= tmp * sin(angle);
				}
#else
				// GPU is based on this, and produces the same digitals.
				for (int k = 0; k < ( KD>>1 ); k++) {   // dim from [L+1,2]
					real angle = At *p[j] * lam[Il(apm,k)];
					real tmp1 = R[IR(apm,k,la)] * R[IR(apm,k,lb)], tmp2 = R[IR(apm,KD-k-1,la)] * R[IR(apm,KD-k-1,lb)];
					//real tmp1 = R[IR(apm,la,k)] * R[IR(apm,lb,k)], tmp2 = R[IR(apm,la,KD-k-1)] * R[IR(apm,lb,KD-k-1)];
					Lr += cos(angle) * ( tmp1 + tmp2 ) ;
					Li -= sin(angle) * ( tmp1 - tmp2 );
				}
				Lr += (KD&1)*(R[IR(apm,(KD>>1), la)] * R[IR(apm,(KD>>1),lb)]);
				//Lr += (KD&1)*(R[IR(apm,la,(KD>>1))] * R[IR(apm,lb,(KD>>1))]);
#endif
				// =========================>>>>

				int lpmj=IF(lb+apm,m+lb-la,j); // Note the index of m in the inner loop may differ from the outer one, even the physical m is the same.
				out_r[lmj] += Lr*in_r[lpmj] - Li*in_i[lpmj];
				out_i[lmj] += Lr*in_i[lpmj] + Li*in_r[lpmj];
			}
		}
	}
}

void pTDSE::calWignerD(real*_RESTRICT_ Wd, real theta_in) {

	for (int l = 0; l <= Lmax; l++)
	for (int m = 0; m <= 2*l;  m++)
	for (int n = 0; n <= 2*l;  n++) {
		int mm=m-l, nn=n-l;
		Wd[IWd(l,m,n)] = FC(djmn)(l,mm,nn,theta_in);
	}
}

//
//  Prepare val matrix R : the large sparse Wigner matrix in (D^l_mn \times F)-space. The D^0_00 is skipped.
// 
void pTDSE::prepareWignerD(real*_RESTRICT_ WdR, real theta_in) {

    const int nnz = pDIM * ( IWd(Lmax+1,0,0)   - 1 );  // total nonzero element in (D^l_mn \times F)-space
    const int n   = pDIM * ( (Lmax+1)*(Lmax+1) - 1 );  // rank of (D^l_mn \times F) matrix

    std::vector<int> cooRowWd_host(nnz);
    std::vector<int> cooColWd_host(nnz);
    std::vector<real> valWd_host(nnz);
	real *valWd_unsort = 0;

#define IWdF(l,m,j)  (((l)*(l)+m-1)*pDIM + j)
	int co=0;
	#pragma omp parallel for
    for (int l = 1; l <= Lmax; l++)   // skip l=0 for D^0_{00}=1
    for (int m = 0; m <= 2*l;  m++)
    for (int n = 0; n <= 2*l;  n++) {
     	int idx = ( IWd(l,m,n) - 1 ) * pDIM; // IWd = (l)*(4*(l)*(l)-1)/3 + (m)*(2*(l)+1) + n
		int mm = m-l, nn = n-l;
		real tmp = FC(djmn)(l,mm,nn,theta_in);
		//printf("--lmn %2d,%2d,%2d = %g\n", l,mm,nn,tmp);
		for(int j=0; j<pDIM; j++) {
		    co++;
		    cooRowWd_host[idx+j] = IWdF(l,m,j);
		    cooColWd_host[idx+j] = IWdF(l,n,j);
		    valWd_host   [idx+j] = tmp;
		}
	}
#undef IWdF
	printf("   Processing sparse WingerD : %d/%d ( %.1f MB ) ... ", nnz,co, nnz*sizeof(real)/(1024*1024.));
	checkcuda(cudaMalloc((void**)&valWd_unsort, nnz*sizeof(real)));
	checkcuda(cudaMalloc((void**)&cooRowWd, nnz*sizeof(int)));
	
    checkcuda(cudaMemcpy(cooRowWd, cooRowWd_host.data(), nnz*sizeof(int), cudaMemcpyHostToDevice));
    checkcuda(cudaMemcpy(cooColWd, cooColWd_host.data(), nnz*sizeof(int), cudaMemcpyHostToDevice));
    checkcuda(cudaMemcpy(valWd_unsort, valWd_host.data(), nnz*sizeof(real), cudaMemcpyHostToDevice));

	// =======>>> Start sorting
    size_t pBufInBytes = 0;
    void *pBuffer = NULL;
    int *P = NULL;

	// step 1: allocate buffer
    status = cusparseXcoosort_bufferSizeExt(handWd, n, n, nnz, cooRowWd,cooColWd, &pBufInBytes);
    if (status != CUSPARSE_STATUS_SUCCESS) { printf("cusparseXcoosort_bufferSizeExt failed\n"); return; }
	
	checkcuda(cudaMalloc( &pBuffer, sizeof(char)*pBufInBytes));
    // step 2: setup permutation vector P to identity
    checkcuda(cudaMalloc( (void**)&P, sizeof(int)*nnz));
    status = cusparseCreateIdentityPermutation(handWd, nnz, P);
    if (status != CUSPARSE_STATUS_SUCCESS) { printf("cusparseCreateIdentityPermutation failed\n"); return; }
    // step 3: sort COO format by Row
    status = cusparseXcoosortByRow(handWd, n, n, nnz, cooRowWd, cooColWd, P, pBuffer);
    if (status != CUSPARSE_STATUS_SUCCESS) { printf("cusparseXcoosortByRow failed\n"); return; }
    // step 4: gather sorted cooVals
#ifdef SINGLE_PRECISION
    status = cusparseSgthr(handWd, nnz, valWd_unsort, WdR, P, CUSPARSE_INDEX_BASE_ZERO);
#else
    status = cusparseDgthr(handWd, nnz, valWd_unsort, WdR, P, CUSPARSE_INDEX_BASE_ZERO);
#endif
    if (status != CUSPARSE_STATUS_SUCCESS) { printf("cusparseDgthr failed\n"); return; }

	
    status = cusparseXcoo2csr(handWd, cooRowWd,nnz, n, csrRowWd,CUSPARSE_INDEX_BASE_ZERO);
    if (status != CUSPARSE_STATUS_SUCCESS) { printf("cusparseXcoo2csr failed\n"); return; }

	checkcuda(cudaFree(cooRowWd));

    checkcuda(cudaFree(pBuffer));
	checkcuda(cudaFree(P));
	checkcuda(cudaFree(valWd_unsort));

	printf("Done.\n");
	
	cooRowWd_host.clear();
    cooColWd_host.clear();
    valWd_host.clear();
}

void pTDSE::RotateState(real*_RESTRICT_ Wd, real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i) {

	// l=0 do not transform, just copy it
	memcpy(out_r, in_r, sizeof(real)*pDIM);
    memcpy(out_i, in_i, sizeof(real)*pDIM);

	#pragma omp parallel for
	for (int l = 1; l <= Lmax; l++) {  //cout << l << " " <<endl;
	for (int m = 0; m <= 2*l;  m++)
	for (int j = 0; j < pDIM; j++) {
		int lmj = IF(l,m,j);
		out_r[lmj] = 0;
		out_i[lmj] = 0;
		for (int n=0; n<=2*l; n++) {
			int lnj = IF(l,n,j);
			real wignerd = Wd[IWd(l,m,n)];
			out_r[lmj] += wignerd * in_r[lnj];
			out_i[lmj] += wignerd * in_i[lnj];
		}
	}}
}

real pTDSE::checkNorm(real*_RESTRICT_ in_r, real*_RESTRICT_ in_i) {
	real norm = 0;
	#pragma omp reduction(+:norm)
	for (int l = 0; l <= Lmax; l++) {  //cout << l << " " <<endl;
	for (int m = 0; m <= 2*l;  m++)
	for (int j = 0; j < pDIM; j++)  {
		int lmj = IF(l,m,j);

		norm += (in_r[lmj]*in_r[lmj]+in_i[lmj]*in_i[lmj]) * dF[j];

	}}
	return norm;
}


/*
void pTDSE::checkInitNorm() {
	real norm;
	#pragma omp reduction(+:norm)
	for (int l = 0; l <= 3; l++) {  //cout << l << " " <<endl;
	for (int n = 0; n < 3;  n++) {
		norm = 0;
		for (int k = 0; k < pDIM; k++) norm += Psi(l,n,k)*Psi(l,n,k)*dF[k];
	}}
	printf("|Psi0|^2 = %.14g\n", norm);
}*/

void printVec(real vec[], int N) {
	for (int i = 0; i < N; i++) {
		printf("%g ", vec[i]);
	}
	printf("\n");
}

void pTDSE::prepareA() {
	 memset(Ar, 0, sizeof(real)*(Lmax+1)*pDIM*pDIM);
	 memset(Ai, 0, sizeof(real)*(Lmax+1)*pDIM*pDIM);

	#pragma omp parallel for
	for (int l = 0; l <= Lmax; l++) {   //cout << omp_get_thread_num() << ":" << l << "  " << endl;
	for (int j = 0; j < pDIM; j++)
	for (int k = 0; k < pDIM; k++) {
		int idx = IA(l, j, k);
		for (int n = 0; n < nDIM; n++) {

			real tmp = Psi(l,n,j)*Psi(l,n,k) * dF[k];

			Ar[idx] += tmp*cos(0.5*dt * Eln(l,n) );
			Ai[idx] -= tmp*sin(0.5*dt * Eln(l,n) );
		}
	} }

}

void pTDSE::prepareDiagCos() {

	const int dim_R   = (Lmax+1)*(Lmax+2)*(2*Lmax+3)/6;  // 1^2 +2^2 + 3^2 + ... + (L+1)^2
	const int dim_lam = ((Lmax+1)*(Lmax+2)) >>1;         // 1+2+...+(L+1)
	
    memset(R,   0, sizeof(real)* dim_R);
    memset(lam, 0, sizeof(real)* dim_lam);

	//real *off2 = new real[Lmax];   // for checking eigen value
	real *off = new real[Lmax];
	real *work = new real[2*Lmax];

	// only for positive m
	for (int m=0; m<Lmax; m++) {  // no need for m=Lmax, whose DIM=1

		int DIM = Lmax-m+1;
		real *Z = R + IR(m,0,0);
		real *D = lam + Il(m, 0);

		// <l1 | cos\theta | l2> = sqrt((l1*l1 - m1*m1) / (4*l1*l1 - 1))
		for (int i=0; i<DIM-1; i++) {   // l in [m,Lmax-1]
			int l2 = (i+m+1)*(i+m+1);
			off[i] = sqrt((l2-m*m)/(4.0*l2-1.0));
		}

		//  Z(i,j) = R^T : i-th eigenvector with index j
		//  where R is the right eigenvetor (MR = RE)
		//
		// DSTEQR store eigenvalue in D from small to large value, which is paired with opsite sign, eg. ...-2 -1 0 1 2... .
		int info;
#ifdef SINGLE_PRECISION
		FC(ssteqr)("I", DIM, D, off, Z, DIM, work, info);
#else
		FC(dsteqr)("I", DIM, D, off, Z, DIM, work, info);
#endif
		if (info!=0) printf("WARNING! m = %2d, Output = %d\n", m, info);

		//printf("m=%d DIM=%d, ir=%d, il=%d \n", m, DIM, IR(m,0,0), Il(m, 0));
		//printf("m=%d : ", m); printVec(D, DIM); printf("\n");

		/* //Check Eigen values
		for (int i=1; i<DIM-1; i++) {
			real tmp = R[IR(m,i,i-1)]*off2[i-1] + R[IR(m,i,i+1)]*off2[i] - D[i]*R[IR(m,i,i)];
			if (tmp>1e-13) printf("m=%d i=%d - %19g\n", m, i, tmp);
		} */


	}
	
#ifdef GPU_RUN
	checkcuda(cudaMemcpy(g_R,   R,   dim_R  *sizeof(real), cudaMemcpyHostToDevice));
    checkcuda(cudaMemcpy(g_lam, lam, dim_lam*sizeof(real), cudaMemcpyHostToDevice));
#endif
	//delete[] off2;
	delete[] off;
	delete[] work;
}


void pTDSE::read_grids() {  // pj, dF_j
    ifstream file;
    char fn[256]; sprintf(fn, "%s/grids.dat", getInPrefix());
    file.open(fn);
    real x;
    for(int i = 0; i < pDIM; i++)  file >> x >> p[i] >> dF[i];
    file.close();
#ifdef GPU_RUN
	checkcuda(cudaMemcpy(g_p,   p, (size_t)pDIM*sizeof(real), cudaMemcpyHostToDevice));
	checkcuda(cudaMemcpy(g_dF, dF, (size_t)pDIM*sizeof(real), cudaMemcpyHostToDevice));
#endif
}

void pTDSE::read_eig() { 	 // Eln(p_j)
    ifstream file;
    char fn[256]; sprintf(fn, "%s/eig.dat", getInPrefix());
    file.open(fn);
    for(int l = 0; l <= Lmax; l++) {
        if (pDIM==500) {
        	int ll;
        	printf("== Skip numbers in 'eig.dat' for Jiang's initial data.\n");
        	file >> ll;
        }
        for(int n = 0; n < nDIM; n++) file >> Eln(l,n);
    }
    file.close();

	//
	//  Determine the bound state, E < Up
	//
	for (int l = 0; l <= Lmax;  l++)
	for (int n = 0; n <  nDIM;  n++) {
		if ( Eln(l,n) >= 0 /*getUp()*/ ) {
			nbound[l] = n;
			//printf("---nbound[%2d] = %d\n", l, n);
			break;
		}
	}
}

//#define WRITE_TO_BINARY
#ifdef WRITE_TO_BINARY
void pTDSE::read_wav() {  // psi_ln(p_j)
    ifstream file;
    char fn[256]; sprintf(fn, "%s/wav.dat", getInPrefix());
    file.open(fn);
	int ll;
    for(int l = 0; l <= Lmax; l++) {
        //file >> ll;
        for(int n = 0; n < nDIM; n++)
        for(int j = 0; j < pDIM; j++)   {
        	file >> Psi(l,n,j);
        	Psi(l,n,j) *= p[j];   // Starr's initial data did not multiply	 p[j]
        }
    }
    file.close();

    cout << "== Save wave functions data into binary format..." << endl;
    sprintf(fn, "%s/wav.bin", getInPrefix());
    std::ofstream of(fn, std::ios::out | std::ios::binary);
    for(int l = 0; l <= Lmax; l++) {
    	of.write((char*)&Psi(l,0,0), pDIM*nDIM*sizeof(real));
    }
    of.close();
}
#else
//
//  Psi_num(l,n,m) = psi*p
//
void pTDSE::read_wav() {  // psi_ln(p_j)*p_j
    ifstream file;
    char fn[256]; sprintf(fn, "%s/wav.bin", getInPrefix());
    file.open(fn, std::ios::in | std::ios::binary);
    for(int l = 0; l <= Lmax; l++) {
    	file.read((char*)&Psi(l,0,0), sizeof(real)*pDIM*nDIM);
    }
    file.close();
}
#endif

void pTDSE::outputATI(char* fn) {
	FILE * f;
	f = fopen(fn, "a");
	fprintf (f, "########### ATI(p_j)   time=%g\n", phy_time);
	for(int j = 0; j < pDIM; j++) {
		fprintf (f, "%g %.14g\n", p[j], ATI[j]);
	}
	fprintf (f, "\n\n");
	fclose(f);
}

void pTDSE::outputState(char* fn, real*_RESTRICT_ in_r, real*_RESTRICT_ in_i) {

    printf("= Output state in %s ...\n", fn);

	const int MAX_OBS_L = 2;

	FILE * f;
	f = fopen(fn, "w");
	fprintf (f, "##     p_j       Re(0, 0)        I(0, 0)        Re(1,-1)        I(1,-1)        Re(1, 0)        I(1, 0)        Re(1, 1)        I(1, 1)        Re(2,-2)        I(2,-2)        Re(2,-1)        I(2,-1)        Re(2, 0)        I(2, 0)        Re(2, 1)        I(2, 1)        Re(2, 2)        I(2, 2)        Re(3,-3)        I(3, -3)        Re(3,-2)        I(3,-2)        Re(3,-1)        I(3,-1)        Re(3, 0)        I(3, 0)        Re(3, 1)        I(3, 1) ...\n");
	for(int j = 0; j < pDIM; j++) {
		fprintf (f, "%13.7g ", p[j]);
		//fprintf (f, "%20.14g ", p[j]);
		for (int l = 0; l <= MAX_OBS_L;  l++)
		for (int m = 0; m <= 2*l;  m++) {
			fprintf (f, "%13.7g %13.7g  ", in_r[IF(l,m,j)], in_i[IF(l,m,j)] );
		}
		fprintf (f, "\n");
	}
    fclose(f);
}
void pTDSE::DumpState(char* fn, real*_RESTRICT_ in_r, real*_RESTRICT_ in_i) {

	printf("= Dump state in %s ...\n", fn);
	FILE * f;
	f = fopen(fn, "w");
	for (int l = 0; l <= Lmax;  l++)
	for(int m = 0; m <= 2*l;  m++) {
		for(int j = 0; j < pDIM; j++) {
			fprintf (f, "%22.14e %22.14e ", in_r[IF(l,m,j)], in_i[IF(l,m,j)] );
		}
		fprintf (f, "\n");
	}

    fclose(f);
}
void pTDSE::DumpBinary(char* fn, real*_RESTRICT_ in_r, real*_RESTRICT_ in_i) {
	printf("= Dump binary in %s ...\n", fn);
	std::ofstream of(fn, std::ios::out | std::ios::binary);
	of.write((char*)in_r, (Lmax+1)*(Lmax+1)*pDIM*sizeof(real));
	of.write((char*)in_i, (Lmax+1)*(Lmax+1)*pDIM*sizeof(real));
	of.close();
}
void pTDSE::outputExact(char* fn) {
	FILE * f;
	f = fopen(fn, "w");
	fprintf (f, "########### StateR(lmj) StateI(lmj)\n");
	fprintf (f, "## l = 0, m = 0\n");
	for(int j = 0; j < pDIM; j++) {
		real psi = sqrt(32.0 / M_PI) / ((1+p[j]*p[j])*(1+p[j]*p[j])) *p[j];
		fprintf (f, "%g %g\n", p[j], psi );
	}
    fclose(f);
}

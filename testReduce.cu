#include<cstdio>
#include <cuda_runtime.h>
#include<cmath>
#include<cstdlib>

#define warpSize 32
#define THREADS 1024

// Warpsum will be saved into the first element.
__inline__ __device__
double warpReduceSum(double val) {
   for (int offset = warpSize>>1; offset > 0; offset /= 2) 
	val += __shfl_down(val, offset);
   return val;
}

__inline__ __device__
double blockReduceSum(double val) {

   static __shared__ double shared[32];
   int lane = threadIdx.x % warpSize;
   int wid  = threadIdx.x / warpSize;
   val = warpReduceSum(val);
   if (lane==0) shared[wid] = val; // 1st thread in each warp write to shared memory
   __syncthreads();              // Wait for all partial reductions

   //read from shared memory only if that warp existed
   val = (threadIdx.x < blockDim.x / warpSize) ? shared[lane] : 0;
   if (wid==0) val = warpReduceSum(val); //Final reduce of first warp

   return val;
}

__global__
void norm_init(double* in, const int N)  {
  for (unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; i < N; i += blockDim.x * gridDim.x) {
    in[i]   = 1;
  }
}

__global__
void norm_kernel(double* in, double* out, const int N)  {
  double sum = 0;
  //reduce multiple elements per thread
  for (unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; i < N; i += blockDim.x * gridDim.x) {
    sum += in[i];
  }
  sum = blockReduceSum(sum);
  //if (threadIdx.x==32)  printf("%g ", sum);
  if (threadIdx.x==0) out[blockIdx.x]=sum;
}


int main() {

	const int N = 1024*1024*512;
	double *in, sum;

        cudaMalloc((void**)&in,  N*sizeof(double));
    
	const int blocks = min( N / THREADS, 1024);
	int griddim = ceil( N/(blocks*THREADS));

	printf("N      : %d\n", N);
	printf("Blocks : %d\n", blocks);
	printf("Grids  : %d\n", griddim );

	norm_init<<<blocks, THREADS>>>(in, N);
	norm_kernel<<<blocks, THREADS>>>(in, in, N);
    
	norm_kernel<<<1, 1024>>>(in, in, blocks);
	
	cudaMemcpy(&sum, in, sizeof(double), cudaMemcpyDeviceToHost);
	printf("Sum = %g / %ld\n", sum, N);

	return 0;
}




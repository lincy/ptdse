#define GPU_RUN


#ifndef PTDSE_HH
#define PTDSE_HH

#define _RESTRICT_ __restrict__
//#define _RESTRICT_ 

#include "global.h"
#include <omp.h>

#include <list>

#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <cusparse_v2.h>
#include "checkcuda.h"

#include "timer.h"

class pTDSE {

	private:
	real CEP, epsilon;
	real Em, wavelen; // nm, Ipeak (in 10^14 w/cm^2)
	int N;

	real phy_time, laserT, omega, dt, At;
	int Lmax, nDIM, pDIM;

	//std::list<real*> *newStat, *oldStat;

	real *f0r, *f0i, *f1r, *f1i;
	real *psi;

	int *nbound;
	real *Eln, *p, *dF;
	real *Ar, *Ai;
	real *R, *lam;

	real *Wd1, *Wd2;   /* Wigner d-matric */
	real *ATI;
	double *Plm;

	// GPU
    real *g_f0r, *g_f0i, *g_f1r, *g_f1i;
	real *g_Ar, *g_Ai, *g_dF;

	real *g_R;    // R[] ~ 200 K
	real *g_lam;  // lam[L^2/2] ~ 6 K 
	real *g_p;    // p[500] ~ 4 K 
	
	cusparseStatus_t status;
	cusparseHandle_t handLam, handWd;
	cublasHandle_t handblas;
	
	cusparseMatDescr_t gMatDescr;    // for WignerD
	int *cooRowWd, *cooColWd, *csrRowWd;
	real *valWd1, *valWd2;
     	
		// for Lambda_GPU
	cusparseMatDescr_t gLamMatD;
    int *cooRowLam, *csrRowLam, *cooColLam;
    real *valLamR, *valLamI;
	real *valLamR_unsort, *valLamI_unsort;
	size_t pBufInBytes_Lam;
	void *pBuf_Lam;
	int *P_Lam;
	int *cooRowLam_host;	  // for Lambda_host
	int *cooColLam_host;
	real *valLamR_host, *valLamI_host;

	char outprefix[256], inprefix[256];

public:
	Timer *main_timer;

    real getDt()    { return dt; }
    real getOmega() { return omega; }
    real getLaserT() { return laserT; }
    real getCEP() { return CEP; }
    real getEpsilon() { return epsilon; }
    real getUp() {  return 0.25*Em*Em/(omega*omega);     }
    real getKeldyshGamma() {  return sqrt(0.25/getUp());     }

    void setInPrefix(char* s) { sprintf(inprefix, "%s", s); }
    void setOutPrefix(char* s) { sprintf(outprefix, "%s", s); }
    char* getInPrefix() {  return inprefix; }
    char* getOutPrefix() { return outprefix; }
	//
	// f_lm(p_k) : pDIM * (L+1)^2
	// A^l_jk    : pDIM^2 * (L+1)
	// Lambda^m_ll : \sum_{m=0}^{L} (L-m+1)^2
	// l^m_k       : \sum_{m=0}^{L} (L-m+1)
	pTDSE(int nd, int ld, int pd, real Em_in, real wv_in, real N_in, real phi_in, real epsilon_in)
	:Lmax(ld), pDIM(pd), nDIM(nd), Em(Em_in), wavelen(wv_in), N(N_in), phy_time(0), CEP(phi_in), epsilon(epsilon_in)
	{
		main_timer = new Timer();
		
		omega = 45.563352529821/wavelen;
		real cycT = 2*M_PI/omega;
		dt    = 0.1; //cycT/(105*8.0);
		laserT = N * cycT;

#define IF(l,m,j)  (((l)*(l)+m)*pDIM + j)    // m=m-l
// DIM for Lambda is \sum_{m=0}^{Lmax} ( (L+1)*(L+2)*(2L+3) - (L-m+1)*(L-m+2)*(2L-2m+3) )/6

#define Il(m,k)      (  (((m)*(2*Lmax+3-(m)) ) >>1) + k )
#define IR(m,l,q)  (  ( (Lmax+1)*(Lmax+2)*(2*Lmax+3) - (Lmax+1-m)*(Lmax+2-m)*(2*(Lmax-m)+3) )/6 + (l)*(Lmax-(m)+1) + q  )

#define IWd(l,m,n)     (   (l)*(4*(l)*(l)-1)/3 + (m)*(2*(l)+1) + n )
#define IA(l,j,k)     (((l)*pDIM + j)*pDIM + k)
#define Ar(l,j,k)  Ar[(((l)*pDIM + j)*pDIM + k)]
#define Ai(l,j,k)  Ai[(((l)*pDIM + j)*pDIM + k)]

#define Eln(l,n)   Eln[(l)*nDIM + n]
#define Psi(l,n,j) psi[((l)*nDIM + n)*pDIM + j]

		// quantum state:
		f0r = new real[(Lmax+1)*(Lmax+1)*pDIM];   // ~ 16 MB  for p=n=1024, L=40
		f0i = new real[(Lmax+1)*(Lmax+1)*pDIM];
		f1r = new real[(Lmax+1)*(Lmax+1)*pDIM];
		f1i = new real[(Lmax+1)*(Lmax+1)*pDIM];

		//newStat = new std::list<real*>({f1r,f1i});
		//oldStat = new std::list<real*>({f0r,f0i});

		psi = new real[(Lmax+1)*nDIM*pDIM];       //  ~400MB for pDIM=1024, L=40

		nbound = new int[(Lmax+1)];
		Eln = new real[(Lmax+1)*nDIM];
		p   = new real[pDIM];
		dF  = new real[pDIM];

		Ar = new real[(Lmax+1)*pDIM*pDIM];                 // ~400MB for pDIM=1024, L=40
		Ai = new real[(Lmax+1)*pDIM*pDIM];
		R   = new real[(Lmax+1)*(Lmax+2)*(2*Lmax+3)/6];    // ~200 KB for L=40
		lam = new real[(Lmax+1)*(Lmax+2)/2];

		Wd1 = new real[(Lmax+1)*(2*Lmax+1)*(2*Lmax+3)/3];   // ~800 KB for L=40
		Wd2 = new real[(Lmax+1)*(2*Lmax+1)*(2*Lmax+3)/3];   // ~800 KB for L=40
		ATI = new real[pDIM];
		Plm = 0x0;
		
#ifdef GPU_RUN
		checkcuda(cudaMalloc((void**)&g_f0r, (Lmax+1)*(Lmax+1)*pDIM*sizeof(real)));
		checkcuda(cudaMalloc((void**)&g_f0i, (Lmax+1)*(Lmax+1)*pDIM*sizeof(real)));
  		checkcuda(cudaMalloc((void**)&g_f1r, (Lmax+1)*(Lmax+1)*pDIM*sizeof(real)));
  		checkcuda(cudaMalloc((void**)&g_f1i, (Lmax+1)*(Lmax+1)*pDIM*sizeof(real)));

	 	checkcuda(cudaMalloc((void**)&g_Ar, (Lmax+1)*pDIM*pDIM*sizeof(real)));
		checkcuda(cudaMalloc((void**)&g_Ai, (Lmax+1)*pDIM*pDIM*sizeof(real)));
        checkcuda(cudaMalloc((void**)&g_dF, pDIM*sizeof(real)));
	
		checkcuda(cudaMalloc((void**)&g_R,   ( (Lmax+1)*(Lmax+2)*(2*Lmax+3)/6 )*sizeof(real)));
		checkcuda(cudaMalloc((void**)&g_lam, ( ((Lmax+1)*(Lmax+2)) >>1 )*sizeof(real)));
        checkcuda(cudaMalloc((void**)&g_p,  pDIM*sizeof(real)));

        status = cusparseCreate(&handWd);
		if (status != CUSPARSE_STATUS_SUCCESS) { printf("cuSparse handle initialization failed\n"); return; }
		status = cusparseCreateMatDescr(&gMatDescr); 
		if (status != CUSPARSE_STATUS_SUCCESS) { printf("Matrix descriptor initialization failed\n"); return; } 
        cusparseSetMatType     (gMatDescr, CUSPARSE_MATRIX_TYPE_GENERAL);
        cusparseSetMatIndexBase(gMatDescr, CUSPARSE_INDEX_BASE_ZERO);

		cublasStatus_t stat = cublasCreate(&handblas);
		if (stat != CUBLAS_STATUS_SUCCESS) { printf("cuBLAS failed-R\n"); return; }

#endif
	}
	~pTDSE() {
		delete[] f0r; delete[] f0i; delete[] f1r; delete[] f1i;
		delete[] psi;
		delete[] nbound;
		delete[] Eln; delete[] p; delete[] dF;
		delete[] Ar; delete[] Ai;
		delete[] R;  delete[] lam;
		delete[] Wd1;delete[] Wd2;
		delete[] ATI;
		if (Plm!= 0x0) delete[] Plm;
		//newStat->clear();
		//oldStat->clear();
	
		main_timer->~Timer();
	
#ifdef GPU_RUN
		// GPU
		cudaFree(g_Ar);  cudaFree(g_Ai);
		cudaFree(g_f0r); cudaFree(g_f0i);  cudaFree(g_f1r); cudaFree(g_f1i);
		cudaFree(g_R); cudaFree(g_lam);  cudaFree(g_p);
	        
		// cublas and cusparse related
		cudaFree(cooRowWd);  cudaFree(cooColWd);   cudaFree(valWd1);   cudaFree(valWd2);
		cusparseDestroy(handWd);
		cublasDestroy(handblas);
		cusparseDestroyMatDescr(gMatDescr);

		cudaDeviceReset(); 
#endif

		fflush (stdout);

	}

	void initF();
	void Evolve();

	void read_eig();    // Eln(p_j)
	void read_grids();  // pj, dF_j
	void read_wav();  // psi_ln(p_j)

	void prepareA();
	void prepareDiagCos();

	void EvolveH0(real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i);
	void EvolveInteraction(real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i);

	void calWignerD(real*_RESTRICT_ WdR, real theta_in);
	void RotateState(real*_RESTRICT_ WdR, real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i);

	void calculateATI(real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i);
	void outputATI(char*);
	void outputState(char* fn, real*_RESTRICT_ in_r, real*_RESTRICT_ in_i);
	void DumpState(char* fn, real*_RESTRICT_ in_r, real*_RESTRICT_ in_i);
	void DumpBinary(char* fn, real*_RESTRICT_ in_r, real*_RESTRICT_ in_i);
	void outputPAD (char* fn, real*_RESTRICT_ in_r, real*_RESTRICT_ in_i);
	void outputPAD2(char* fn, real*_RESTRICT_ in_r, real*_RESTRICT_ in_i);
	void outputExact(char* fn);

	real checkNorm(real*_RESTRICT_ in_r, real*_RESTRICT_ in_i);
	void checkInitNorm();
	void Wav2Host();

	// GPU
	void gPrepareA();
	void gEvolveH0     (real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i);
	void gEvolveInt    (real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i);
    void gRotateState  (real*_RESTRICT_ WdR,  real*_RESTRICT_ in_r, real*_RESTRICT_ in_i,  real*_RESTRICT_ out_r, real*_RESTRICT_ out_i);
 	real gCheckNorm    (real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i);
	void prepareWignerD(real*_RESTRICT_ WdR, real theta);

private:
	void swapState(real **in_r, real **in_i, real **out_r, real **out_i) {
		real *tmp;
		tmp=*in_r; *in_r=*out_r; *out_r=tmp;
		tmp=*in_i; *in_i=*out_i; *out_i=tmp;
	}
};


#endif

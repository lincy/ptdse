#ifndef CHECKCUDA_H
#define CHECKCUDA_H
	
#define checkcuda(x) {  \
  cudaError_t err = x;  \
  if (err != cudaSuccess) {  \
    fprintf(stderr, "CUDA Runtime Error: %s in %s:%d\n", cudaGetErrorString(err), __FILE__,__LINE__); \
	exit(0); \
  }          \
}


#endif

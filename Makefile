include Makefile.gpu

TARGET  = tdse 
OBJECTS = tdse.o wigner_d.o plm.o pTDSE.o pTDSE_PAD.o pTDSE_PAD2.o pTDSE_norm.o pTDSE_evolve.o pTDSE_A.o

###########################################################
all: $(TARGET)

$(TARGET):$(OBJECTS)
	$(NVCC) $(NVCFLAG) $(OBJECTS) -o $@ $(LIBS)

run:
	#./tdse
	export OMP_NUM_THREADS=8; time ./tdse
	##/usr/local/cuda-7.0/bin/nvprof --system-profiling on --print-gpu-trace -o tdse.trace ./tdse

clean:
	rm *.o $(TARGET) -f
	

##########################################################
testWignerD : testWignerD.o wigner_d.o
	$(CXX) $(OPT) testWignerD.o wigner_d.o -o $@ -L/usr/lib64 -llapack -lgfortran  -lcuda
testPlm : testPlm.o plm_Tong.o plm.o
	$(CXX) $(OPT) testPlm.o plm_Tong.o plm.o -o $@ -L/usr/lib64 -llapack -lgfortran  -lcuda

testReduce : testReduce.o
	$(NVCC) $(NVCFLAG) $< -o $@ $(LIBS)

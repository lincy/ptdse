#include "pTDSE.h"
#include <cuda_runtime.h>
#include "checkcuda.h"

//
// This reduction works for any pDIM <= 1024
//

#define BLOCK_DIM 1024
#define warpSize 32

__inline__ __device__
real warpReduceSum(real val) {
  for (int offset = warpSize/2; offset > 0; offset /= 2) 
    val += __shfl_down(val, offset);
  return val;
}

__device__
real blockReduceSum(real val) {

  static __shared__ real shared[32]; // assume block dim = 1024 = 32*32
  int lane = threadIdx.x % warpSize;
  int wid  = threadIdx.x / warpSize;
  val = warpReduceSum(val);     // partial reduction in each wrap
  if (lane==0) shared[wid]=val; // 1st thread in each wrap write to shared memory
  __syncthreads();              // Wait for all partial reductions

  //read from shared memory only if that warp existed
  val = (threadIdx.x < blockDim.x / warpSize) ? shared[lane] : 0;
  if (wid==0) val = warpReduceSum(val); //Final reduce of first warp

  return val;
}

__global__ 
void norm_vec(	const real*_RESTRICT_ rpart, const real*_RESTRICT_ ipart, real*_RESTRICT_ work, 
		const int N, const real*_RESTRICT_ dF, const int dfdim)  {
	unsigned int tid = threadIdx.x;
	// Wavefunction norm vector; assume gridDim=1
	for (unsigned int i = blockIdx.x * dfdim + threadIdx.x; i < N; i += dfdim * gridDim.x) {
		if (tid < dfdim )  // I use this trick to avoid dF=500 != 512
			work[i] += ( rpart[i]*rpart[i] + ipart[i]*ipart[i] ) * dF[tid];

	}
}

__global__
void reduce(const real*_RESTRICT_ in, real*_RESTRICT_ out, const int N)  {
  real sum = 0;
  //reduce multiple elements per thread
  for (unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; i < N; i += blockDim.x * gridDim.x) {
    sum += in[i];
  }
  sum = blockReduceSum(sum);
  if (threadIdx.x==0) out[blockIdx.x]=sum;
}

//
//  TODO: check with a slower reduction routine
//
#ifndef WITHOUT_SHIFT_OP
real pTDSE::gCheckNorm( real*_RESTRICT_ rp,  real*_RESTRICT_ ri,
                        real*_RESTRICT_ w1,  real*_RESTRICT_ w2   ) {
   const int N = (Lmax+1)*(Lmax+1)*pDIM;
   const int blocks = min( (N + BLOCK_DIM - 1) / BLOCK_DIM, 1024);

   cudaMemset(w1, 0, N*sizeof(real)); 	
   norm_vec<<<blocks, BLOCK_DIM>>>(rp, ri, w1, N, g_dF, pDIM);
   
   reduce<<<blocks, BLOCK_DIM>>>(w1, w2, N);   // sum for each block stored in the out[blockId], 
   reduce<<<1, 1024>>>(w2, w2, blocks);      

   real sum;
   cudaMemcpy(&sum, w2, sizeof(real), cudaMemcpyDeviceToHost);
   return sum;
}
#else

//
//  NOT working
//
__global__ 
void total(real* in, real* out, int LEN) {
    static __shared__ real sdata[1024];

	real sum = 0;
	unsigned int tid = threadIdx.x;
	//reduce multiple elements per thread
	for (unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; i < LEN; i += blockDim.x * gridDim.x) {
		sdata[tid] = in[i];
		sum += in[i];
	}

	__syncthreads();

	for (unsigned int s=1; s < blockDim.x; s *= 2) {
		int index = 2 * s * tid;
		if (index < blockDim.x) sdata[index] += sdata[index + s];
		__syncthreads();
	}

	if (tid == 0) out[blockIdx.x] = sdata[0];
}

real pTDSE::gCheckNorm(real*_RESTRICT_ rp,  real*_RESTRICT_ ri, real*_RESTRICT_ w1,  real*_RESTRICT_ w2   ) {
   const int N = (Lmax+1)*(Lmax+1)*pDIM;
   const int blocks = min( (N + BLOCK_DIM - 1) / BLOCK_DIM, 1024);
   
   norm_vec<<<blocks, BLOCK_DIM>>>(rp, ri, w1, N, g_dF, pDIM);

	int nBlock = int(N/1024. + 0.5);

	total<<<nBlock, 1024>>>(w1, w2, N);

   real sum;
   cudaMemcpy(&sum, w2, sizeof(real), cudaMemcpyDeviceToHost);
   return sum;
}
#endif

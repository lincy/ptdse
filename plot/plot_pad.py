#!/usr/bin/python

import os
import numpy as np

#from scipy.interpolate import griddata
#import itertools

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt



##dt = np.dtype("2i4, 500f8, f8")
f = open("PAD.dat", "rb")
dim = np.fromfile(f, np.dtype('i'), count=2)
p   = np.fromfile(f, np.dtype('d'), count=500)
data = np.fromfile(f, np.dtype('d'))

#Create source grid
#tet = np.linspace(0.0, 2*np.pi*(1-1./360), 360)
tet = np.linspace(0.0, 2*np.pi, 360)
pp=np.linspace(0,100,500)
t_mesh, p_mesh = np.meshgrid(tet,pp)

#pp = np.array( [list(itertools.chain.from_iterable(pts[0])), list(itertools.chain.from_iterable(pts[1]))] ).T
d=np.reshape(data, (np.size(tet),np.size(p))).T


fig, ax = plt.subplots(figsize=(10,10), subplot_kw=dict(projection='polar'))
ax.set_theta_zero_location("N")
ax.set_theta_direction(-1)
ax.contourf(t_mesh, p_mesh, d )
fig.savefig('PAD.png', bbox_inches='tight')
#plt.show()




set term post color enhanced

set xr[0:20]
set yr[:]

set xlabel "p(au)"
set ylabel "Re 00 mode"

set output "Exact.ps"
plot  \
  "512/StateI.dat" u 1:($2) lw 2 w l t  "512", \
  "500/StateI.dat" u 1:(-$2) lw 2 w l t "500", \
  "1024/StateI.dat"  u 1:($2) lw 2 w l t "1024", \
  "500/StateExact.dat" u 1:($2) lw 2 w l t  "p * exact = sqrt(32/PI) / ((1+p*p)^2)"
plot  \
  "512/aaa" u 1:($2-$4) lw 2 w l t "512-exact", \
  "500/aaa" u 1:($2+$4) lw 2 w l t "500-exact", \
  "1024/aaa" u 1:($2-$4) lw 2 w l t "1024-exact"



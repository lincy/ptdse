set term post color enhanced

set xr[0:20]
set yr[:]

set xlabel "p(au)"
set ylabel "State 00"

F1="Sp0500I.dat"
F2="Sp0512I.dat"

set output "CompareState.ps"

set title "Iteration=0"
plot  \
  "500/State0000.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "500", \
  "512/State0000.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "512", \
  "1024/State0000.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "1024"

set title "Iteration=500"
plot  \
  "500/State0500.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "500", \
  "512/State0500.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "512", \
  "1024/State0500.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "1024"

set title "Iteration=1000"
plot  \
  "500/State1000.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "500", \
  "512/State1000.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "512", \
  "1024/State1000.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "1024"

set title "Iteration=1500"
plot  \
  "500/State1500.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "500", \
  "512/State1500.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "512", \
  "1024/State1500.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "1024"

set title "Iteration=2500"
plot  \
  "500/State2500.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "500", \
  "512/State2500.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "512", \
  "1024/State2500.dat" u 1:(sqrt($2**2+$3**2)) lw 3 w l t "1024"


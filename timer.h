#include <sys/param.h>
#include <sys/times.h>
#include <sys/types.h>

//
// Timer of real/user/system time
//
class Timer {
   private:
      struct tms t,u;
      float saveu, saves, saver;
      long r1,r2;

   public :
      Timer()      { Reset();  }
      void Reset() { saveu = 0; saves = 0; saver = 0; }
      void Start() { r1 = times(&t);   }
      void Stop()  {
            r2 = times(&u);
            saveu += u.tms_utime-t.tms_utime;
            saves += u.tms_stime-t.tms_stime;
            saver += float(r2-r1);
      }
      float ctime()   { return saveu/HZ; } // usertime
      float systime() { return saves/HZ; }
      float wtime()   { return saver/HZ; } // walltime
};


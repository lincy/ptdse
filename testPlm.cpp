#include "global.h"
#include <cstdio>
#include <cstring>
#include <cmath>


int main (void)
{
	int Lmax=5;
	real rad = 0.3*M_PI;
	real cosx = cos(rad);

    double* Plm = new double[((Lmax+1)*(Lmax+2))>>1];
    Plm_value( rad, Lmax, Plm);

    for (int l=0;l<=Lmax;l++) {
	printf("L=%d : ", l);
        for (int m=0;m<=l;m++) {
	    real a = Plm[((l*(l+1))>>1)+m];
	    real b = FC(plgndr)(l, m, cosx);
	    printf("%5g ", a-b);
	}
	printf("\n");
    }

	printf("\n");
    nPlm_value( rad, Lmax, Plm);
    for (int i=0;i< ((Lmax+1)*(Lmax+2))>>1;i++) {
	    real a = Plm[i];
	    printf("%5g ", a);
    }
	printf("\n");


    int a= 100>>1;    
    printf("%5d\n", a);

    return 0;
}


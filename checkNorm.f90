program main

real(8) :: psi(500,500,0:40)
real(8) :: x(500), p(500), dF(500), norm1, norm2
integer :: l, n, tmp

open(1,file='/home/lincy/tdse_dat/wav.dat')
do l = 0,40
    read(1, *) tmp, psi(:,:,l)
    print *, "Reading l = ", tmp
enddo

open(2,file='/home/lincy/tdse_dat/grids.dat')
do i = 1,500
    read(2, *) x(i), p(i), dF(i)
enddo


do l = 0,3
do n = 1,3
do ll = 0,3
do nn = 1,3
    print *, l,n,ll,nn, " = ", sum(psi(:,n,l)*psi(:,nn,ll)*dF(:) )

!print *, "l=",l, "psi*psi *dF = ", norm1, norm2
enddo
enddo
enddo
enddo


!print *, ""
!print *, "psi*psi *p*p*p*p *dF = ", norm2


close(2)
close(1)

end program

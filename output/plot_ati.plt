set term post color enhanced
set output "ati.ps"

set xr[0:2]
set yr[:]

set xlabel "p(au)"
set ylabel "ATI" 

FILE="ATI.dat"
plot  \
  FILE u 1:2 index 0 lw 3 w l,  \
  FILE u 1:2 index 1 lw 3 w l,  \
  FILE u 1:2 index 2 lw 3 w l,  \
  FILE u 1:2 index 3 lw 3 w l


set ylabel "State(00)"
set output "state_00.ps"
plot [:20] [:] \
  "StateI.dat"    u 1:2 lw 3 w l t "Init",  \
  "StateFGPU.dat" u 1:2 lw 3 w l t "Final Re", \
  "StateFGPU.dat" u 1:3 lw 3 w l t "Final Im"

set ylabel "State(10)"
set output "state_10.ps"
plot [:20] [:] \
  "StateI.dat"    u 1:2 lw 3 w l t "Init",  \
  "StateFGPU.dat" u 1:2 lw 3 w l t "Final", \
  "StateFGPU.dat" u 1:3 lw 3 w l t "Final"


!
!  pTDSE for circular polarization by Jiang
!

! ifort -O3 -qopenmp -parallel      for linux
        use omp_lib
        implicit none
      integer (kind=4),parameter :: dp=kind(1.d0)
      integer, parameter :: n=500, Lm=35,Lm2=Lm*2
      real (kind=dp), parameter :: pi= 3.1415926535897d0
      real(kind=dp), allocatable :: x(:),p(:),df(:),en(:,:),ef(:,:,:), &
	    xj(:),xy(:),xjp(:),xyp(:),sb(:,:)
      real(kind=dp) :: omg,total,At,gamma,cep
      real(kind=dp) :: Up,ta,sm,sm1,sm2,tmp,plgndr,F3J
      real(kind=dp) :: Em,fs, dt, wv, EGc, cyc
      integer :: Ldel, ncyc, nt, NB(0:Lm)
      integer :: it,i,j,L,L2, k,L1,ierr,m,m1,mdim,i1,i2,IFAIL
      real(kind=dp) :: time1,time2,ti3,ti4,djmn,xl,xl1,xl2,xm,xn
      complex (kind=dp),parameter :: zero =cmplx(0.d0,0.d0),zi=cmplx(0.d0,1.d0)
      complex (kind=dp) :: ztmp,zsum
      complex (kind=dp),allocatable,dimension(:) ::  zu,zv
      complex (kind=dp),allocatable,dimension(:,:) :: zw
      complex (kind=dp),allocatable,dimension(:,:,:) :: zf,zg,zs
!--------------------------------------------------------------------
      open(1,file='grids.dat')
      open(2,file='eig.dat')
      open(3,file='wav.dat')
      open(60,file='time.txt')
      open(70,file='finalwav.dat')
  write ( *, '(a,i8)' ) 'number of processors available = ', omp_get_num_procs ( )
  write ( *, '(a,i8)' ) 'number of threads available    = ', omp_get_max_threads ( )
	call cpu_time(time1)
    call TIMEL(ti3)
    print *,' start time :',time1
      write(60,*) ' n, Lm'
      write(60,*) n,Lm+1
        fs=  100.d0/2.418884326505d0
! IR : wvlength=wv nm, Ipeak (in 10^14 w/cm^2),phi=CEP
      cep= -0.5d0*pi
      wv= 400.d0
      omg= 45.5633525d0/wv
      cyc= 2.d0*pi/omg
      Em= sqrt(0.25d0/351.d0)
      Up= Em**2/omg**2/4.d0
      total= 1.d0*cyc
         dt= cyc/(105.d0*8.d0)
         nt= total/dt
     gamma= sqrt(0.5d0*0.5/Up)
   print *,' wvl(nm),Up,Keldysh,T/cyc,dt,nt= ',wv,Up,gamma,total/cyc,dt,nt
   write(60,*) ' wvl(nm),Up,Keldysh,T/cyc,dt,nt= ',wv,Up,gamma,total/cyc,dt,nt
  allocate(x(n),p(n),df(n),en(n,0:Lm),ef(n,n,0:Lm),stat=ierr)
      do i=1,n
      read(1,*) x(i),p(i),df(i)
      enddo
      print *,'pmin,pmax= ',p(1),p(n)
! read output of 3dh.f90 :
      do k=0,Lm
         read(2,*) Ldel
         read(2,100) (en(i,k),i=1,n)
      enddo
! read output of 3dh.f90 ; f(p_j,ith,L)-->ef
      do k=0,Lm
         read(3,*) Ldel
         do i=1,n
          read(3,100) (ef(j,i,k),j=1,n)
         enddo
      enddo
      close(1); close(2); close(3)
! number of bound states each L
      write(60,*) '  L   # of bound_levels '
      do k=0,Lm
      	NB(k)= 0
      	do i=1,n
      	if(en(i,k).LT.0.d0) NB(k)= NB(k) + 1
!         if(en(i,k) < Up) NB(k)=NB(k)+1
        enddo
!          print *,' L, NB= ',k,NB(k)
          write(60,*) k,NB(k),en(Nb(k),k)
      enddo
! check normalization for L=0, first few lower states
!$omp parallel do REDUCTION(+:sm) private(i)
       do j=1,3
       sm= 0.d0
        do i=1,n
        sm=sm + df(i)*ef(i,j,0)**2
        enddo
       print *,j,sm
       enddo
!   The S(n,n;L) matrix :   exp[-idt*H_0/2]
       EGc= 2.d0*pi/dt
      print *,' EGc= ',EGc
      write(60,*) ' EGC= ',EGc
    allocate(zs(n,n,0:Lm),stat=ierr)
      do L=0,Lm
      do i=1,n
      do j=1,n
      	zsum= zero
        DO 2 k=1,n
          if(en(k,L) > EGc) goto 2
          ztmp= cmplx(cos(en(k,L)*dt/2.d0),-sin(en(k,L)*dt/2.d0))
          zsum= zsum + ef(i,k,L)*ztmp*ef(j,k,L)*df(j)
  2     continue
       zs(i,j,L)= zsum
      enddo
      enddo
      enddo 
! initial prepared in 1s state :
      allocate(zf(n,0:Lm,-Lm:Lm),zg(n,0:Lm,-Lm:Lm),stat=ierr)
      zf(:,:,:)= zero
      do i=1,n
      zf(i,0,0)= cmplx(ef(i,1,0),0.d0)
      enddo
       write(60,*) 'cut-off order ~ ',(-en(1,0)+3.2*Up)/omg
! time evolution
      print *,'      it       L       m  (L,m)-norm    '
!   allocate(fr(n,0:Lm,-Lm:Lm),fi(n,0:Lm,-Lm:Lm),stat=ierr)
 allocate(xj(0:Lm2),xjp(0:Lm2),xy(0:Lm2),xyp(0:Lm2),sb(n,0:Lm2),stat=ierr)



 do 10 it=0,3
         ta= dt*it + dt/2.d0
	     At= -dt*Em*sin(pi*ta/total)**2/omg

 print *,it, "T=",ta, " At=", At 
 	  
		 
! exp[-idt*H_0/2] :
   zg(:,:,:)= zero
       DO m=-Lm,Lm         
         do L=IABS(m),Lm
	     call zmatrix_vec(n,zs(1,1,L),zf(1,L,m),zg(1,L,m) )
         enddo
       ENDDO 
! Wigner rotation 
   DO L=0,Lm
    xl= dble(L)
    DO m=-L,L
       xm= dble(m)
     DO i=1,n
     zsum= zero
      DO m1=-L,L
       xn= dble(m1)
      zsum= zsum + djmn(xl,xn,xm,omg*dt/2.d0)*zg(i,L,m1)
      ENDDO
           zf(i,L,m)= zsum
     ENDDO
    ENDDO
   ENDDO
! dipole-interaction part :
   DO i=1,n
   xj=0.d0; xjp=0.d0; xy=0.d0; xyp=0.d0
   tmp= At/dt*p(i)
	if(tmp >= 0.d0) then
	 call  SBESJY(tmp,Lm2,xj,xy,xjp,xyp,IFAIL)
     else
	 call SBESJY(-tmp,Lm2,xj,xy,xjp,xyp,IFAIL)
	  DO L=0,Lm2
	  xj(L)=  (-1.d0)**L*xj(L)
	  xjp(L)= (-1.d0)**L*xjp(L)
	  xy(L)=  (-1.d0)**L*xy(L)
	  xyp(L)= (-1.d0)**L*xyp(L)
	  ENDDO
    endif
    	DO L=0,Lm2  	
	    sb(i,L)= xj(L)
	    ENDDO
   ENDDO
!  
     DO L=0,Lm
	 xl=dble(L)  
	 DO m=-L,L
	 xm=dble(m)
	  DO i=1,n
      zsum= zero

	  DO L1=0,Lm
	  xl1=dble(L1)	  
	   DO  3 L2=IABS(L-L1),L+L1  
	   if(mod((L+L1+L2),2) .NE. 0) goto 3
	   xl2=dble(L2)
       zsum=zsum+zi**L2*(-1.d0)**(L2+m)*dble(2*L2+1)*sqrt(dble((2*L+1)*(2*L1+1))) &
	   *F3J(xl,xl2,xl1,0.d0,0.d0,0.d0)*F3J(xl,xl2,xl1,-xm,0.d0,xm)*sb(i,L2)*zf(i,L1,m)
  3    continue	 
       ENDDO

      zg(i,L,m)= zsum
	  ENDDO
	 ENDDO
     ENDDO   
!  Wigner rotation :
  DO L=0,Lm
     xl= dble(L)
    DO m=-L,L
    xm= dble(m)
     DO i=1,n
     zsum= zero
      DO m1=-L,L
      xn= dble(m1)
      zsum= zsum + djmn(xl,xn,xm,omg*dt/2.d0)*zg(i,L,m1)
      ENDDO
     zf(i,L,m)= zsum
     ENDDO
    ENDDO
   ENDDO
! ! exp[-idt*H_0/2] :
       DO m=-Lm,Lm         
        do L=IABS(m),Lm
	 call zmatrix_vec(n,zs(1,1,L),zf(1,L,m),zg(1,L,m) )
        enddo
       ENDDO
	     zf(:,:,:)= zg(:,:,:) 
   if(mod(it,105*2) == 0)  then
!$omp parallel do private(sm,L)
      DO L=0,Lm
	  DO m=-L,L  
      sm= 0.d0 
	DO i=1,n
	sm= sm + df(i)*abs(zf(i,L,m))**2
	ENDDO
	 if(sm .GT. 0.0001) then
       print *,it,L,m,sm	 
       write(60,*) it,L,m,sm
     endif
      enddo
	  enddo
 
          endif
10     continue
!-------------------------------------------------------------------

!$omp parallel do private(sm,L)
      DO L=0,Lm
	  DO m=-L,L  
      sm= 0.d0 
	DO i=1,n
	sm= sm + df(i)*abs(zf(i,L,m))**2
	ENDDO
	 if(sm .GT. 0.0001) then
       print *,it,L,m,sm	 
       write(60,*) it,L,m,sm
     endif
      enddo
	  enddo

!      write(70,*) Lm,wv,omg,Up
      DO L=0,Lm
	  DO m=-L,L  	  
       DO i=1,n
       write(70,100) p(i),real(zf(i,L,m)),imag(zf(i,L,m))
       ENDDO
      write(70,*)
      ENDDO
      ENDDO
! extract the bound part (br,bi) and conti (cr,ci) :
     
 9     print *,'cut-off order ~ ',(-en(1,0)+3.2*Up)/omg
       call cpu_time(time2)
	write(60,*) 'cpu_time (s)= ',time2-time1
	print *, 'cpu time (s) : ',time2-time1
	call TIMEL(ti4)
    print *,' wall time(s) : ',ti4-ti3
100   format(6(1x,1pe14.7))
101   format(2(1x,I5),2(1x,1pe14.7))
102   format(8(1x,1pe12.5))
      stop
      END
!
    SUBROUTINE TIMEL(TRES)
! Returns CPU time.  Fortran 9X version                          *
      REAL*8 TRES,TRES0
      SAVE IF,TRES0,IRES0
      DATA IF/0/
! THIS IS INTRINSIC PROCEDURE IN FORTRAN<90>
     IF(IF.EQ.0)CALL SYSTEM_CLOCK(IRES0,IRESR,IRESM)
     IF=1
     CALL SYSTEM_CLOCK(IRES,IRESR,IRESM)
     TRES=DBLE(IRES-IRES0)/DBLE(IRESR)
      END
!-----------------------------------------------------------------------
      Subroutine zmatrix_vec(ndim, a, b, x)
! return x(:)= (matrix a)*b(:), complex-matrix times complex-vector
      implicit none
      integer :: ndim,i,j
      integer(kind=4),parameter :: dp=kind(1.d0)
      complex (kind=dp) :: a(ndim,ndim), b(ndim), x(ndim)
      x(:) = cmplx(0.d0, 0.d0)
!$OMP PARALLEL DO Private(j)
      do i=1, ndim
         do j=1, ndim
            x(i) = x(i) + a(i,j) * b(j)
         end do
      end do
      return
      end
!-----------------------------------------------------------------------
	Subroutine dmatrix_vec(ndim, a, b, x)
! return x(:)= (matrix a)*b(:), real-matrix times real-vector
	implicit none
	integer :: ndim,i,j
	integer(kind=4),parameter :: dp=kind(1.d0)
	real (kind=dp) :: a(ndim,ndim), b(ndim), x(ndim)
	x(:) = 0.d0
!$OMP PARALLEL DO Private(j)
	do i=1, ndim
	   do j=1, ndim
		x(i) = x(i) + a(i,j) * b(j)
	   end do
	end do
	return
	end
!
        function plgndr(l,m,x)       result(value)
        implicit none
 !
 !      compute the associated Legendre polynomial P_l^m(x), Here
 !      M and l are integers satisfying 0 <= m <= l, while x lies in
 !      -1 <= x <= 1
 !                        code copied from Numerical Recipes
 !                        by Xiao-Min Tong, Mar. 28, 1997, KU
 !
        integer, parameter :: dp=kind(1.d0)
        integer (kind=4) :: l,m, i, ll
        real (kind=dp) :: value, x
        real (kind=dp) :: fact,pll,pmm,pmmp1,somx2

        if(m.lt.0.or.m.gt.l.or. dabs(x).gt.1.d0) then
         write(6,*) ' Bad arguments in plgndr ',l,m,x
         stop
        end if
 !
 !------------------ compute PMM
        pmm = 1.d0
        if(m.gt.0) then
          somx2 = dsqrt( (1.d0+x)*(1.d0-x) )
          fact = 1.d0
          do i=1,m
            pmm = -pmm*fact*somx2
            fact = fact + 2.d0
          end do
        end if
        if(l.eq.m) then
          value  = pmm
        else
          pmmp1 = x*dble(2*m+1)*pmm
          if(l.eq.(m+1)) then
            value  = pmmp1
          else
            do ll=m+2,l
             pll = (x*dble(2*ll-1)*pmmp1-dble(ll+m-1)*pmm)/dble(ll-m)
             pmm = pmmp1
             pmmp1 = pll
            end do
            value  = pll
          end if
        end if
      end function plgndr
!------------------------------------------from NR ---------------------
      Subroutine gauleg(x1,x2,x,w,n)
      Implicit none
      Integer(kind=4) :: n,i,j,m
      integer(kind=4),parameter :: dp=kind(1.d0)
      REAL(kind=dp) :: x1,x2,x(n),w(n)
      Real(kind=dp), Parameter :: EPS = 3.d-14,pi=3.1415926535897d0
      Real(kind=dp) :: p1,p2,p3,pp,xl,xm,z,z1
!-----------------------------------------------------------------------
      m  = (n+1)/2
      xm = 0.5d0 * (x2 + x1)
      xl = 0.5d0 * (x2 - x1)
      do i = 1, m
        z = dcos(pi* (i - 0.25d0) / (n + 0.5d0))
1       continue

         p1=1.d0
         p2=0.d0

         do j=1,n
            p3=p2
            p2=p1
            p1=((2.d0*j-1.d0)*z*p2-(j-1.d0)*p3)/j
         end do

         pp = n * (z*p1-p2)/(z*z-1.d0)
         z1 = z
         z  = z1-p1/pp

         if( dabs(z-z1) .gt. EPS) goto 1

         x(i)     = xm - xl * z
         x(n+1-i) = xm + xl * z
         w(i)     = 2.d0 * xl / ((1.d0 - z * z) * pp * pp )
         w(n+1-i) = w(i)
      end do
      End  SUBROUTINE gauleg
!
  function djmn (j, m, n, beta_rad)
  implicit none
! -------------------------------------------------------------------
! input: angular momentum quantum numbers j, m, n (all real)
!        beta_rad = Euler angle beta (radian)
! -------------------------------------------------------------------
  integer, parameter :: wp = kind(1.0d0)  ! working precision = double (portable)
!--------------------------------------------------------------------
!    formal arguments
!--------------------------------------------------------------------
  real(wp), intent(in) :: j, m, n, beta_rad
!--------------------------------------------------------------------
!    local variables
!--------------------------------------------------------------------
  integer :: itmin1, itmin2, itmin, itmax1, itmax2, itmax, ij1, ij2, &
             it, iphase, ia, ib, ic
  real(wp) :: djmn, cosb2, sinb2, sqrt_fac, sumt, denom, term
!--------------------------------------------------------------------
!   external functions
!--------------------------------------------------------------------
  real(wp), external :: fac10           ! computes factorial(n)/10**n
!--------------------------------------------------------------------
!  program starts here
!--------------------------------------------------------------------
  cosb2 = cos(beta_rad/2.0_wp)
  sinb2 = sin(beta_rad/2.0_wp)
!--------------------------------------------------------------------
! determine lower and upper limits for summation index it; these
! are derived from the requirement that all factorials n! in the
! denominator are restricted to values with n >=0.
!--------------------------------------------------------------------
  itmin1 = 0
  itmin2 = nint(m-n)
  itmin = max(itmin1,itmin2)
  itmax1 = nint(j+m)
  itmax2 = nint(j-n)
  itmax = min(itmax1,itmax2)
!  write (6,'(10X,A,2I6)') ' itmin, itmax = ', itmin, itmax
  ij1 = nint(j-m)
  ij2 = nint(j+n)
  sqrt_fac = sqrt( fac10(itmax1) * fac10(ij1) * fac10(ij2) * fac10(itmax2) )
!
  sumt = 0.0_wp
  do it = itmin, itmax
     iphase = (-1)**it
     ia = itmax1 - it
     ib = itmax2 - it
     ic = it + nint(n-m)
!     write (6,'(10X,A,5I6)') ' it, iphase, ia, ib, ic  = ', it, iphase, ia, ib, ic
     denom = fac10(ia) * fac10(ib) * fac10(it) * fac10(ic)
     term = iphase * cosb2**(ia+ib) * sinb2**(it+ic) / denom
     sumt = sumt + term
  end do
  djmn = sqrt_fac * sumt
!
  return
  end function djmn


  function fac10 (n)
  implicit none
! -----------------------------------------------
! function fac10(n) calculates factorial(n)/10**n
! -----------------------------------------------
! input: integer n >= 0 (you may want to check this
!        in the program calling this function)
! -----------------------------------------------
  integer, parameter :: wp = kind(1.0d0)  ! working precision = double (portable)
!------------------------------------------------
!      formal arguments
!------------------------------------------------
  integer, intent(in) :: n
!------------------------------------------------
!      local variables
!------------------------------------------------
  integer :: i
  real(wp) :: fac10, q
! -----------------------------------------------
  if (n == 0) then
     fac10 = 1.0_wp
  else
     fac10 = 1.0_wp
     q = 1.0_wp
     do i = 1, n
        fac10 = fac10 * q / 10.0_wp
        q = q + 1.0_wp
     end do
  endif
!
  return
  end function fac10
      SUBROUTINE FACLOG
!#######################################################################
!#    INITIALISATION OF LOGARITHMS OF FACTORIALS ARRAY                 #
!#######################################################################
      IMPLICIT none
      integer(kind=4), parameter :: dp = kind(1.d0)
      real(kind=dp) :: FCT
      integer(kind=4) :: NTIMES,I,AI
      COMMON /LOGFAC/ FCT(1000)
      DATA NTIMES /0/
!
      NTIMES = NTIMES+1
      IF (NTIMES .GT. 1) RETURN
      FCT(1) = 0.D0
      DO 10 I = 1,999
         AI = I
         FCT(I+1) = FCT(I)+DLOG(DBLE(AI))
 10   CONTINUE
!
      RETURN
      END
!
      FUNCTION F3J(FJ1,FJ2,FJ3, FM1,FM2,FM3) 
!#######################################################################
!#    CALCULATES 3J COEFFICIENTS FROM RACAH FORMULA                    #
!#    (MESSIAH: T2, P 910; FORMULA 21) .                               #
!#    CLEBSCH-GORDAN COEFFICIENTS ARE GIVEN BY (P. 908, FORMULA 12) :  #
!#                         J -J +M                !J    J     J!       #
!#    <J J M M !J M> = (-1) 1  2   (2*J+1)**(0.5) ! 1    2     !       #
!#      1 2 1 2                                   !M    M    -M!       #
!#                                                ! 1    2     !       #
!#---------------------------------------------------------------------#
!#    HAS BEEN TESTED FOR J UP TO 200.                                 #
!#    LOGFAC SHOULD CONTAIN THE LOGARITHMS OF FACTORIALS               #
!#    J.M.L. (1975)                                                    #
!#######################################################################
      IMPLICIT none
      integer(kind=4), parameter :: dp = kind(1.d0)
      real(kind=dp) :: X,TINY,ZERO,ONE,FCT,FJ1,FJ2,FJ3,FM1,FM2,FM3, &
       FK1,FK2,FK3,FK4,FK5,FK1M,FK2M,FK1P,FK2P,K1,K2,K3,K4,K5, &
       N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,CC,PHASE,FSP,F3J
      INTEGER(kind=4) ::  T,TMIN,TMAX,NTIMES,NS
      DATA TINY,ZERO,ONE /0.01D0,0.D0,1.D0/ ,NTIMES /1/
      COMMON /LOGFAC/ FCT(1000)
      IF (NTIMES .EQ. 1) CALL FACLOG
      NTIMES = NTIMES+1
      CC = ZERO
      IF (FJ3 > (FJ1+FJ2+TINY))      GO TO 100
      IF (abs(FJ1-FJ2) .GT. (FJ3+TINY)) GO TO 100
      IF (abs(FM1+FM2+FM3) .GT. TINY)    GO TO 100
      IF (abs(FM1) .GT. (FJ1+TINY))      GO TO 100
      IF (abs(FM2) .GT. (FJ2+TINY))      GO TO 100
      IF (abs(FM3) .GT. (FJ3+TINY))      GO TO 100
      FK1 = FJ3-FJ2+FM1
      FK2 = FJ3-FJ1-FM2
      FK3 = FJ1-FM1
      FK4 = FJ2+FM2
      FK5 = FJ1+FJ2-FJ3
      FK1M = FK1-TINY
      FK2M = FK2-TINY
      FK1P = FK1+TINY
      FK2P = FK2+TINY
      IF (FK1M .LT. ZERO) K1 = FK1M
      IF (FK1P .GT. ZERO) K1 = FK1P
      IF (FK2M .LT. ZERO) K2 = FK2M
      IF (FK2P .GT. ZERO) K2 = FK2P
      K3 = FK3+TINY
      K4 = FK4+TINY
      K5 = FK5+TINY
      TMIN = 0
      IF (K1+TMIN .LT. 0) TMIN = -K1
      IF (K2+TMIN .LT. 0) TMIN = -K2
      TMAX = K3
      IF (K4-TMAX .LT. 0) TMAX = K4
      IF (K5-TMAX .LT. 0) TMAX = K5
      N1 = FJ1+FJ2-FJ3+ONE+TINY
      N2 = FJ2+FJ3-FJ1+ONE+TINY
      N3 = FJ3+FJ1-FJ2+ONE+TINY
      N4 = FJ1+FM1+ONE+TINY
      N5 = FJ2+FM2+ONE+TINY
      N6 = FJ3+FM3+ONE+TINY
      N7 = FJ1-FM1+ONE+TINY
      N8 = FJ2-FM2+ONE+TINY
      N9 = FJ3-FM3+ONE+TINY
      N10 = FJ1+FJ2+FJ3+2.D0+TINY
      X = FCT(N1)+FCT(N2)+FCT(N3)+FCT(N4)+FCT(N5)+FCT(N6) &
        +FCT(N7)+FCT(N8)+FCT(N9)-FCT(N10)
      X = 0.5D0*X
      DO 10  T = TMIN,TMAX
         PHASE = ONE
         IF (MOD(T,2) .NE. 0) PHASE = -ONE
         CC = CC+PHASE*exp(-FCT(T+1)    -FCT(K1+T+1)-FCT(K2+T+1) &
                        -FCT(K3-T+1)-FCT(K4-T+1)-FCT(K5-T+1)+X)
 10   CONTINUE
      FSP = abs(FJ1-FJ2-FM3)+TINY
      NS = IDNINT(FSP)
      IF (MOD(NS,2) > 0) CC = -CC
 100  F3J = CC
      RETURN
      END FUNCTION F3J
!---------------------------------------------------------------------
        SUBROUTINE SBESJY  (X,LMAX, J,Y,JP,YP, IFAIL )
!C---------------------------------------------------------------------
!C   REAL SPHERICAL BESSEL FUNCTIONS AND X DERIVATIVES
!C            j , y , j', y'                    FROM L=0 TO L=LMAX
!C        FOR REAL X > SQRT(ACCUR) (E.G. 1D-7)    AND INTEGER LMAX
!C 
!C  J (L)  =      j/L/(X) STORES   REGULAR SPHERICAL BESSEL FUNCTION:
!C  JP(L)  = D/DX j/L/(X)            j(0) =  SIN(X)/X
!C  Y (L)  =      y/L/(X) STORES IRREGULAR SPHERICAL BESSEL FUNCTION:
!C  YP(L)  = D/DX y/L/(X)            y(0) = -COS(X)/X
!C                                                
!C    IFAIL = -1 FOR ARGUMENTS OUT OF RANGE
!C          =  0 FOR ALL RESULTS SATISFACTORY
!C 
!C   USING LENTZ-THOMPSON EVALUATION OF CONTINUED FRACTION CF1,
!C   AND TRIGONOMETRIC FORMS FOR L = 0 SOLUTIONS.
!C   LMAX IS LARGEST L NEEDED AND MUST BE <= MAXL, THE ARRAY INDEX.
!C   MAXL CAN BE DELETED AND ALL THE ARRAYS DIMENSIONED (0:*)
!C   SMALL IS MACHINE DEPENDENT, ABOUT SQRT(MINIMUM REAL NUMBER),
!C         SO 1D-150 FOR DOUBLE PRECISION ON VAX, PCS ETC.
!C   PRECISION:  RESULTS TO WITHIN 2-3 DECIMALS OF "MACHINE ACCURACY"
!C   IN OSCILLATING REGION X .GE.  [ SQRT{LMAX*(LMAX+1)} ]
!C   I.E. THE TARGET ACCURACY ACCUR SHOULD BE 100 * ACC8 WHERE ACC8
!C   IS THE SMALLEST NUMBER WITH 1+ACC8.NE.1 FOR OUR WORKING PRECISION
!C   THIS RANGES BETWEEN 4E-15 AND 2D-17 ON CRAY, VAX, SUN, PC FORTRANS
!C   SO CHOOSE A SENSIBLE  ACCUR = 1.0D-14
!C   IF X IS SMALLER THAN [ ] ABOVE THE ACCURACY BECOMES STEADILY WORSE:
!C   THE VARIABLE ERR IN COMMON /STEED/ HAS AN ESTIMATE OF ACCURACY.
!C
!C   NOTE: FOR X=1 AND L=100  J = 7.4 E-190     Y = -6.7+E186    1.4.94
!C---------------------------------------------------------------------
!C   AUTHOR :   A.R.BARNETT       MANCHESTER    12 MARCH 1990/95
!C                                AUCKLAND      12 MARCH 1991
!C---------------------------------------------------------------------  
        IMPLICIT    NONE
        integer(kind=4)  :: LMAX, IFAIL, NFP, L
        integer(kind=4),parameter :: LIMIT = 20000, MAXL = 1001 
        real(kind=8) :: J(0:MAXL), Y(0:MAXL), JP(0:MAXL), YP(0:MAXL), &
                    ACCUR, TK,SL, ERR,      &
                    X,XINV, CF1,DCF1, DEN, C,D, OMEGA, TWOXI
        real(kind=8),PARAMETER :: ZERO = 0.0D0, ONE  = 1.0D0 ,TWO = 2.0D0, &
         SMALL = 1.D-150, THREE = 3.0D0 
!        COMMON /STEDE/    ERR,NFP       ! not required in code        
!-------
        ACCUR = 1.D-14                  ! suitable for double precision
        IFAIL = -1                      ! user to check on exit
        IF (X .LT. DSQRT(ACCUR) )       GOTO 50
!C-------TAKES CARE OF NEGATIVE X ... USE REFLECTION FORMULA
!C-------BEGIN CALCULATION OF CF1 UNLESS LMAX = 0, WHEN SOLUTIONS BELOW
            XINV  = ONE / X
        IF (LMAX .GT. 0) THEN
            TWOXI =     XINV + XINV
            SL  =  REAL(LMAX)* XINV     ! used also in do loop 3
            TK  =  TWO * SL  + XINV * THREE     
            CF1 =  SL                   ! initial value of CF1
            DEN =  ONE                  ! unnormalised j(Lmax,x)
            IF ( ABS(CF1) .LT. SMALL ) CF1 = SMALL
            C   = CF1                   ! inverse ratio of A convergents
            D   = ZERO                  ! direct  ratio of B convergents   
            DO 10 L = 1,LIMIT
                C   = TK - ONE / C
                D   = TK - D
                        IF ( ABS(C) .LT. SMALL ) C = SMALL
                        IF ( ABS(D) .LT. SMALL ) D = SMALL
                D   = ONE / D
                DCF1= D   * C
                CF1 = CF1 * DCF1
                        IF ( D .LT. ZERO ) DEN = - DEN
                        IF ( ABS(DCF1 - ONE) .LE. ACCUR ) GOTO 20
               TK   = TK + TWOXI
               NFP  = L                 ! ie number in loop
   10       CONTINUE
                    GOTO 50             ! error exit, no convergence
   20       CONTINUE
            ERR = ACCUR * DSQRT(DBLE(NFP))    ! error estimate
                    J (LMAX) = DEN      ! lower-case j's  really
                    JP(LMAX) = CF1 * DEN                         
!C------ DOWNWARD RECURSION TO L=0  AS SPHERICAL BESSEL FUNCTIONS
            DO 30  L =  LMAX , 1, -1
                   J (L-1)  = (SL + XINV) * J(L)   + JP(L)
                        SL  =  SL - XINV
                   JP(L-1)  =  SL * J(L-1)          - J(L)
   30       CONTINUE
            DEN = J(0)
        ENDIF                           ! end loop for Lmax GT 0
!C------ CALCULATE THE L=0 SPHERICAL BESSEL FUNCTIONS DIRECTLY
        J (0)   =  XINV * DSIN(X)
        Y (0)   = -XINV * DCOS(X)
        JP(0)   = -Y(0) - XINV * J(0)
        YP(0)   =  J(0) - XINV * Y(0)
        IF (LMAX .GT. 0) THEN
                   OMEGA  =  J(0) / DEN
                      SL  = ZERO
            DO 40 L = 1 , LMAX
                    J (L) = OMEGA * J (L)
                    JP(L) = OMEGA * JP(L)
                    Y (L) = SL * Y(L-1)   -   YP(L-1)
                      SL  = SL + XINV
                    YP(L) = Y(L-1)  -  (SL + XINV) * Y(L)
   40       CONTINUE
        ENDIF
        IFAIL = 0                       ! calculations successful
        RETURN
!C---------------------------------------------------------------------
!C       ERROR TRAPS
!C---------------------------------------------------------------------
   50   IF (X .LT. ZERO) THEN
                WRITE(6,1000) X
          ELSEIF (X .EQ. ZERO) THEN
                IFAIL = 0
                J(0) = ONE
                DO 60 L = 1, LMAX
                J(L) = ZERO     ! remaining arrays untouched
   60           CONTINUE            
          ELSE  
          J(0)= 1.d0    ! need j_n only
          do L=1,Lmax
          J(L)= X*J(L-1)/dble(2*L+1)
          if(abs(J(L)) < SMALL) J(L)=SMALL
          enddo	
!          	J(0)= 1.d0-X**2/6.d0
!          	J(1)= X/3.d0-X**3/30.d0
!          	JP(0)=-X/3.d0
!          	Y(0)= -1.d0/X-X/2.d0
!          	Y(1)= -1.d0/X**2-1.d0/6.d0
!          	YP(0)= 1.d0/X**2-0.5d0
         	
!          	do L=1,Lmax
!          	JP(L)= -(1.d0+L)*J(L)/X + J(L-1)	
!          	YP(L)= -(1.d0+L)*Y(L)/X + Y(L-1)
!          	enddo	
!          	do L=2,Lmax
!            J(L)= (2.d0*L-1.d0)*J(L-1)/X - J(L-2)
!            Y(L)=	(2.d0*L-1.d0)*Y(L-1)/X - Y(L-2)
!            enddo          	          	       	         	              
!                WRITE(6,1001) X  ! x .le. sqrt(accur), e.g. 1D-7
          ENDIF
 1000   FORMAT(' X NEGATIVE !',1PE15.5,'    USE REFLECTION FORMULA'/)
 1001   FORMAT(' WITH X = ',1PE15.5,'    TRY SMALL-X SOLUTIONS',/,  &
        '    j/L/(X)  ->   X**L / (2L+1)!!          AND',/,    &
        '    y/L/(X)  ->  -(2L-1)!! / X**(L+1)'/)
        RETURN
        END


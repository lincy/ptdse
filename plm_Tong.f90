       function plgndr(l,m,x)       result(value)
       implicit none
!
!      compute the associated Legendre polynomial P_l^m(x), Here
!      M and l are integers satisfying 0 <= m <= l, while x lies in
!      -1 <= x <= 1
!                        code copied from Numerical Recipes
!                        by Xiao-Min Tong, Mar. 28, 1997, KU
!
       integer, parameter :: dp=kind(1.d0)
       integer (kind=4) :: l,m, i, ll
       real (kind=dp) :: value, x
       real (kind=dp) :: fact,pll,pmm,pmmp1,somx2

       if(m.lt.0.or.m.gt.l.or. dabs(x).gt.1.d0) then
        write(6,*) ' Bad arguments in plgndr ',l,m,x
        stop
       end if
!
!------------------ compute PMM
       pmm = 1.d0
       if(m.gt.0) then
         somx2 = dsqrt( (1.d0+x)*(1.d0-x) )
         fact = 1.d0
         do i=1,m
           pmm = -pmm*fact*somx2
           fact = fact + 2.d0
         end do
       end if
       if(l.eq.m) then
         value  = pmm
       else
         pmmp1 = x*dble(2*m+1)*pmm
         if(l.eq.(m+1)) then
           value  = pmmp1
         else
           do ll=m+2,l
            pll = (x*dble(2*ll-1)*pmmp1-dble(ll+m-1)*pmm)/dble(ll-m)
            pmm = pmmp1
            pmmp1 = pll
           end do
           value  = pll
         end if
       end if
     end function plgndr

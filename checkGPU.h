


void checkGPU() {

  int nDevices;
  cudaError_t cudaStat;

  cudaStat = cudaGetDeviceCount(&nDevices);
  if (cudaSuccess != cudaStat) {
         printf("cudaGetDevice failed: cudaStat %d, %s\n", cudaStat, cudaGetErrorString(cudaStat));
         return;
  }
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
    printf("Device Number: %d\n", i);
    printf("  Device name: %s\n", prop.name);
    printf("  Memory Clock Rate (KHz): %d\n",
           prop.memoryClockRate);
    printf("  Memory Bus Width (bits): %d\n",
           prop.memoryBusWidth);
    printf("  Peak Memory Bandwidth (GB/s): %f\n",
           2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
    printf("  Compute capability %d\n\n", 100*prop.major + 10*prop.minor);

  }
  printf("\n\n");
}

#include "pTDSE.h"
#include <fstream>

void pTDSE::outputPAD(char* fn, real *in_r, real *in_i) {
int Lmax1=18;
    printf("= Output PAD binary in %s ...\n", fn);

    const int N_THETA_PI = 180;

    if (Plm == 0x0) {
    	Plm = new double[((Lmax+1)*(Lmax+2))>>1];
    }

    real *psir = new real[2*N_THETA_PI*pDIM];
    real *psii = new real[2*N_THETA_PI*pDIM];
    //
    // tet in [0,pi), phi=0
    //
	for (int i = 0; i < N_THETA_PI; i++) {  nPlm_value( i * M_PI / N_THETA_PI ,Lmax, Plm);
	#pragma omp parallel for
	for (int j = 0; j < pDIM;  j++) {
		int idx = i*pDIM+j;
		psir[idx] = 0;
		psii[idx] = 0;
		for (int l = 0; l <= Lmax1; l++) {
			{
			//real tmp = Plm[(l*(l+1))>>1] * sqrt(l+0.5);
			real tmp = Plm[(l*(l+1))>>1];
			psir[idx] += tmp * in_r[IF(l,l,j)];
			psii[idx] += tmp * in_i[IF(l,l,j)];
			}
			for (int m = 1; m <= l;  m++) {
				//real tmp = sqrt( 0.5*(2*l+1)*factorial(l-m)/factorial(l+m) ) * Plm[m+((l*(l+1))>>1)];
				real tmp = Plm[m + ((l*(l+1))>>1) ];
				psir[idx] += tmp * ( pow(-1,m) * in_r[IF(l,l-m,j)] + in_r[IF(l,l+m,j)]  );
				psii[idx] += tmp * ( pow(-1,m) * in_i[IF(l,l-m,j)] + in_i[IF(l,l+m,j)]  );
			}
		}
	} }

    //
    // tet in [pi,2*pi), phi=pi
    //
	for (int i = N_THETA_PI; i < 2*N_THETA_PI; i++) { nPlm_value( (2*N_THETA_PI-i) * M_PI / N_THETA_PI ,Lmax, Plm);
	#pragma omp parallel for
	for (int j = 0; j < pDIM;  j++) {
		int idx = i*pDIM+j;
		psir[idx] = 0;
		psii[idx] = 0;
		for (int l = 0; l <= Lmax1; l++) {
			{
			//real tmp = Plm[(l*(l+1))>>1] * sqrt(l+0.5);
			real tmp = Plm[(l*(l+1))>>1];
			psir[idx] += tmp * in_r[IF(l,l,j)];
			psii[idx] += tmp * in_i[IF(l,l,j)];
			}
			for (int m = 1; m <= l;  m++) {
				//real tmp = sqrt( 0.5*(2*l+1)*factorial(l-m)/factorial(l+m) ) * Plm[m+((l*(l+1))>>1)];
				real tmp = Plm[m + ((l*(l+1))>>1)];
				psir[idx] += tmp * ( pow(-1,m) * in_r[IF(l,l+m,j)] + in_r[IF(l,l-m,j)]  );
				psii[idx] += tmp * ( pow(-1,m) * in_i[IF(l,l+m,j)] + in_i[IF(l,l-m,j)]  );
			}
		}
	} }
#if 0
	FILE * f;
	f = fopen(fn, "w");

	// Calculate dP / (dp dtht)
	for (int i = 0; i < 2*N_THETA_PI; i++) {
	for (int j = 0; j < pDIM;  j++) {
		int idx = i*pDIM+j;
		//psir[idx] = (psir[idx]*psir[idx] + psii[idx]*psii[idx]) / p[j] ;
		psir[idx] = (psir[idx]*psir[idx] + psii[idx]*psii[idx]);
		fprintf (f, "%10.4g ", psir[idx] );
	} 
		fprintf (f, "\n");
	}
	fclose(f);
#else
	#pragma omp parallel for
	for (int i = 0; i < 2*N_THETA_PI; i++)
	for (int j = 0; j < pDIM;  j++) {
		int idx = i*pDIM+j;
		//psir[idx] = (psir[idx]*psir[idx] + psii[idx]*psii[idx]) / p[j] ;
		psir[idx] = (psir[idx]*psir[idx] + psii[idx]*psii[idx])  / (p[j]*p[j])  ;
	}

	std::ofstream file;
	file.open(fn, std::ios::out | std::ios::binary);
	int dim[] = {pDIM, pDIM*2*N_THETA_PI };
	file.write((char*)dim,   sizeof(int)*2);
	file.write((char*)p,    sizeof(real)*dim[0]);
	file.write((char*)psir, sizeof(real)*dim[1]);
	file.close();
#endif

	delete[] psir;
	delete[] psii;
}


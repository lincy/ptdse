/*
 ============================================================================
 Name        : \Lambda in Step 2
 Author      : Chun-Yu Lin
 ============================================================================
 */

#include <cstdio>
#include <cstring>
#include <cmath>

typedef double real;

#define FC(x) x##_
extern "C" {
	void dsteqr_(char*, int&, real*, real*, real*, int&, real*, int&);
}

#define IDX(i,j) ((i)*N+j)

int main (void)
{
	int Lmax=40;
	int info;

	real *dia = new real[Lmax+1];
	memset(dia, 0, sizeof(real)*(Lmax+1));
	real *off = new real[Lmax];

	real *work = new real[2*Lmax];
	real *Z = new real[(Lmax+1)*(Lmax+1)];

	for (int m=0; m<Lmax; m++) {

		int dim = Lmax-m+1;
		memset(dia, 0, sizeof(real)*dim);

		// <l1 | cos\theta | l2> = sqrt((l1*l1-m1*m1)/(4*l1*l1-1.0))
		for (int i=0; i<dim-1; i++) {   // l in [m,Lmax-1]
			int l2 = (i+m+1)*(i+m+1);
			off[i] = offbk[i] = sqrt((l2-m*m)/(4.0*l2-1.0));
		}

		//
		//  Z(i,j) = R^T : i-th eigenvector with index j
		//  where R is the right eigenvetor (MR = RE)
		FC(dsteqr)("I", dim, dia, off, Z, dim, work, info);
		printf("m = %2d, Output = %d\n", m, info);
	
		checkMVEV(offbk, Z, dia, dim);
	}

	delete[] dia;
	delete[] off;
	delete[] Z;
	delete[] work;

    return 0;
}


! -----------------------------------------------------------------------
! Test of function subprogram djmn which calculates the
! "reduced Wigner rotation matrix" d^j_{m n} (beta).
! The test program calculates matrices d^j_{m n} (beta) for
! j-values 1/2, 1, 3/2, and 2; and also one case for j=12.
! -----------------------------------------------------------------------
! Ref: D.M. Brink and G.R. Satchler, Angular Momentum, 2nd ed.,
!      Oxford University Press, in particular
!      p.22 (general expression for djmn)
!      p.24 (analytic expressions for j = 1/2, 1, 3/2, and 2)
! -----------------------------------------------------------------------
! author: V. Oberacker, Vanderbilt University
! last time edited: October 6, 2010
! -----------------------------------------------------------------------
  function djmn (j, m, n, beta_rad)
  implicit none
! -------------------------------------------------------------------
! input: angular momentum quantum numbers j, m, n (all real)
!        beta_rad = Euler angle beta (radian)
! -------------------------------------------------------------------
  integer, parameter :: wp = kind(1.0d0)  ! working precision = double (portable)
!--------------------------------------------------------------------
!    formal arguments
!--------------------------------------------------------------------
  real(wp), intent(in) :: beta_rad
  integer, intent(in) :: j, m, n
!--------------------------------------------------------------------
!    local variables
!--------------------------------------------------------------------
  integer :: itmin1, itmin2, itmin, itmax1, itmax2, itmax, ij1, ij2, &
             it, iphase, ia, ib, ic
  real(wp) :: djmn, cosb2, sinb2, sqrt_fac, sumt, denom, term
!--------------------------------------------------------------------
!   external functions
!--------------------------------------------------------------------
  real(wp), external :: fac10           ! computes factorial(n)/10**n
!--------------------------------------------------------------------
!  program starts here
!--------------------------------------------------------------------
  cosb2 = cos(beta_rad/2.0_wp)
  sinb2 = sin(beta_rad/2.0_wp)
!--------------------------------------------------------------------
! determine lower and upper limits for summation index it; these
! are derived from the requirement that all factorials n! in the
! denominator are restricted to values with n >=0.
!--------------------------------------------------------------------
  itmin1 = 0
  itmin2 = m-n
  itmin = max(itmin1,itmin2)
  itmax1 = j+m
  itmax2 = j-n
  itmax = min(itmax1,itmax2)
!  write (6,'(10X,A,2I6)') ' itmin, itmax = ', itmin, itmax
  ij1 = j-m
  ij2 = j+n
  sqrt_fac = sqrt( fac10(itmax1) * fac10(ij1) * fac10(ij2) * fac10(itmax2) )
!
  sumt = 0.0_wp
  do it = itmin, itmax
     iphase = (-1)**it
     ia = itmax1 - it
     ib = itmax2 - it
     ic = it + (n-m)
!     write (6,'(10X,A,5I6)') ' it, iphase, ia, ib, ic  = ', it, iphase, ia, ib, ic
     denom = fac10(ia) * fac10(ib) * fac10(it) * fac10(ic)
     term = iphase * cosb2**(ia+ib) * sinb2**(it+ic) / denom
     sumt = sumt + term
  end do
  djmn = sqrt_fac * sumt
!
  return
  end function djmn


  function fac10 (n)
  implicit none
! -----------------------------------------------
! function fac10(n) calculates factorial(n)/10**n
! -----------------------------------------------
! input: integer n >= 0 (you may want to check this
!        in the program calling this function)
! -----------------------------------------------
  integer, parameter :: wp = kind(1.0d0)  ! working precision = double (portable)
!------------------------------------------------
!      formal arguments
!------------------------------------------------
  integer, intent(in) :: n
!------------------------------------------------
!      local variables
!------------------------------------------------
  integer :: i
  real(wp) :: fac10, q
! -----------------------------------------------
  if (n == 0) then
     fac10 = 1.0_wp
  else
     fac10 = 1.0_wp
     q = 1.0_wp
     do i = 1, n
        fac10 = fac10 * q / 10.0_wp
        q = q + 1.0_wp
     end do
  endif
!
  return
  end function fac10

/*
 ============================================================================
 Name        : tdse.c
 Author      : Chun-Yu Lin
 ============================================================================
 */

#include <cstdio>
#include <cstring>
#include <cmath>

typedef double real;

#define FC(x) x##_
extern "C" {
	void dsteqr_(char*, int&, real*, real*, real*, int&, real*, int&);
}

#define IDX(i,j) ((i)*N+j)
void checkMVEV(real M[], real V[], real E[], int N) {
	for (int j = 0; j < N; j++) {
	    real sum = 0.0 + M[0]*V[IDX(j,1)] - E[j]*V[IDX(j,0)];
	    for (int i = 1; i < N-1; i++) {
	        sum+= M[i-1]*V[IDX(j,i-1)] + M[i]*V[IDX(j,i+1)] - E[j]*V[IDX(j,i)];
	    }
	    sum += M[N-2]*V[IDX(j,N-2)] + 0.0 - E[j]*V[IDX(j,N-1)];
    	    if (std::abs(sum) > 1e-13) printf ("FAIL : %g !!\n ", sum);
	}
}
void checkEV(real mat[], int N) {
	for (int j = 0; j < N; j++) {
	real sum=0;
	for (int i = 0; i < N; i++) {
	    	sum+= mat[j*N + i]*mat[j*N + i];
	    	//sum+= mat[i*N + j]*mat[i*N + j];
	}
		if (sum-1.0 > 1e-14) printf ("FAIL=============================== %g !!\n ", sum);
	}
}
void printEV(real mat[], int N) {
	for (int i = 0; i < N; i++) printf("%g ", mat[i*N+1]);
	printf("\n");
	for (int i = 0; i < N; i++) printf("%g ", mat[1*N+i]);
	printf("\n");
}
void printMat(real mat[], int N) {
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++)
		{
	    	printf ("%g ", mat[i*N + j]);
		}
		printf ("\n ");
	}
}
void printVec(real vec[], int N) {
	for (int i = 0; i < N; i++) {
		printf ("%g ", vec[i]);
	}
	printf ("\n");
}

int main (void)
{
	int Lmax=15;
	int info;

	real *dia = new real[Lmax+1];
	memset(dia, 0, sizeof(real)*(Lmax+1));
	real *off = new real[Lmax];
	real *offbk = new real[Lmax];

	real *work = new real[2*Lmax];
	real *Z = new real[(Lmax+1)*(Lmax+1)];

	for (int m=0; m<Lmax; m++) {
	//for (int m=10; m<11; m++) {

		int dim = Lmax-m+1;
		memset(dia, 0, sizeof(real)*dim);

		// <l1 | cos\theta | l2> = sqrt((l1*l1-m1*m1)/(4*l1*l1-1.0))
		for (int i=0; i<dim-1; i++) {   // l in [m,Lmax-1]
			int l2 = (i+m+1)*(i+m+1);
			off[i] = offbk[i] = sqrt((l2-m*m)/(4.0*l2-1.0));
		}

		//
		//  Z(i,j) = R^T : i-th eigenvector with index j
		//  where R is the right eigenvetor (MR = RE)
		FC(dsteqr)("I", dim, dia, off, Z, dim, work, info);
		printf("m = %2d, Output = %d\n", m, info);
	
		//checkMVEV(offbk, Z, dia, dim);
		//checkEV(Z, dim);

		printMat(Z, dim);


		printf("m=%d : ", m);
		printVec(dia, dim);
		printf("\n");
	}

	delete[] dia;
	delete[] off;
	delete[] Z;
	delete[] work;

    return 0;
}






/*
void printGSLVec(gsl_vector *vec) {
	int N = vec->size;
	for (int i = 0; i < N; i++)  {
	    printf ("%g ", gsl_vector_get (vec, i));
	}
}
void printGSLMat(gsl_matrix *mat) {
	int N1 = mat->size1;
	int N2 = mat->size2;
	for (int i = 0; i < N1; i++) {
		for (int j = 0; j < N1; j++)
		{
	    	printf ("%g ", gsl_matrix_get (mat, i, j) );
		}
		printf ("\n ");
	}
}
*/

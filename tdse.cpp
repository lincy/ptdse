#include "pTDSE.h"
#include "checkGPU.h"

#include "cuda_profiler_api.h"

int main (void)
{
	//=======================  Input parameter : users should edit here only ==================
	real wavelength = 800.0;  // w=0.057  ~  800nm laser,
	real Em  = sqrt(1.06/351.0);  //  I = 1.06 e14 W/cm2 / 3.51e16 Intensity in atomic unit
	real CEP     = -M_PI/2;
	real epsilon =  M_PI/2; //M_PI/2.,  11*M_PI/25, M_PI/3., M_PI/4.
	int  Lmax= 40,  N = 3;

	//int  pDIM=1024, nDIM=1024;
	int pDIM=512, nDIM=512;
	//int  pDIM=500, nDIM=500;
	char inprefix[256]; 	sprintf(inprefix,  "/home/lincy/inputdata/%d", pDIM);
	char outprefix[256];	sprintf(outprefix, "/home/lincy/gpu_tdse/test%d"  ,pDIM);    // should mkdir first
	//=========================================================================================

#ifdef GPU_RUN
	checkGPU();
#endif
	Timer timer;
	timer.Start();

	// initial main TDSE class
	pTDSE tdse(nDIM, Lmax, pDIM, Em, wavelength, N, CEP, epsilon);
	tdse.setInPrefix(inprefix);
	tdse.setOutPrefix(outprefix);

	printf(	"=========== pTDSE split-operator solver ===========\n"
			"   Lmax,n,j = %d %d %d\n"
			"   WV(nm) = %10g  Em    = %10g  N  = %d  CEP     = %g\n"
			"   LaserT = %10g  omega = %10g  dt = %g  epsilon = %g\n"
			"   Up     = %10g  KeldyshGamma = %g      Steps = %.0f\n\n"
			"   Threads : %d / %d\n\n",
		   Lmax,nDIM,pDIM,wavelength, Em, N, CEP,
		   tdse.getLaserT(), tdse.getOmega(),  tdse.getDt(), tdse.getEpsilon(),
		   tdse.getUp(), tdse.getKeldyshGamma(), tdse.getLaserT()/tdse.getDt(),
		   omp_get_max_threads(), omp_get_num_procs() );

	cout << "== Reading eig / grid / wave functions data ..." << endl;
	tdse.read_eig();
	tdse.read_grids();
	tdse.read_wav();

	tdse.main_timer->Start();
	tdse.initF();

	char fn[256];
	sprintf(fn, "%s/StateExact.dat", tdse.getOutPrefix());
	tdse.outputExact(fn);

	cout << "== Prepare for Lambda Eigensystem..." << endl;
	tdse.prepareDiagCos();
	
	cout << "== Preparing A ..." << endl;
#ifdef GPU_RUN
	tdse.gPrepareA();
#else
	tdse.prepareA();
#endif
	printf("====== Iteration begins ...\n");

	cudaProfilerStart();
	tdse.Evolve();
	cudaProfilerStop();

	timer.Stop();
	cout << "Done...  Wall/CPU Time: " << timer.wtime() << " / " << timer.ctime() << endl;

    return 0;
}


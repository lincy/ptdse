#include "pTDSE.h"
#include <fstream>

void pTDSE::outputPAD2(char* fn, real *in_r, real *in_i) {

    printf("= Output PAD binary in %s ...\n", fn);
    const int N_THETA = 360;
    if (Plm == 0x0) Plm = new double[((Lmax+1)*(Lmax+2))>>1];
    real *res = new real[N_THETA*pDIM];

	for (int i = 0; i < N_THETA; i++) {  nPlm_value( i*2*M_PI / N_THETA ,Lmax, Plm);
	int fa = int(i >= (N_THETA>>1));
#pragma omp parallel for
	for (int j = 0; j < pDIM;  j++) {
		real psir = 0;
		real psii = 0;
		for (int l = 0; l <= Lmax; l++) {
			for (int m = 0; m <= 2*l;  m++) {
				//real tmp = sqrt( 0.5*(2*l+1)*factorial(l-m)/factorial(l+m) ) * Plm[m+((l*(l+1))>>1)];
				real tmp = Plm[abs(l-m) + ((l*(l+1))>>1) ] * ((1-fa)+fa*pow(-1,m-l));
				psir += tmp * in_r[IF(l,m,j)];
				psii += tmp * in_i[IF(l,m,j)];
			}
		}
		res[i*pDIM+j] = (psir*psir + psii*psii) / p[j] ;
	} }

	// write to binary
	std::ofstream file;
	file.open(fn, std::ios::out | std::ios::binary);
	int dim[] = {pDIM, pDIM*N_THETA };
	file.write((char*)dim,   sizeof(int)*2);
	file.write((char*)p,   sizeof(real)*dim[0]);
	file.write((char*)res, sizeof(real)*dim[1]);
	file.close();

	delete[] res;
}


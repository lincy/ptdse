#include "pTDSE.h"
#include <cuda_runtime.h>
#include "checkcuda.h"

__global__
void gEvolveIntKernel(const real*_RESTRICT_ in_r, const real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i, 
	const real At, const real*_RESTRICT_ p, const real*_RESTRICT_ lam, const real*_RESTRICT_ R, const int pDIM) {

	int Lmax = gridDim.x-1;
	int l = blockIdx.x;
	int m = blockIdx.y;
	int j = threadIdx.x;

	if (m>2*l || j>=pDIM) return;   // skip part of blocks
	
	//
	//  Lambda_{ll'} = \sum_k R_{kl} * cos(A_0 p_j delta E_k )    * R_{kl'}
	//
		int apm = abs(m - l);  // absolute value of physical m
		int KD = Lmax-apm+1;
		int la = l-apm;   // l in term of {m}-space

			int lmj = IF(l,m,j);
			out_r[lmj] = 0;
			out_i[lmj] = 0;
			
			// reduction.....
			for (int lb = 0; lb < KD; lb++) {

				// <<<<=========================
				// This subloop calcuate Lambda^{jm}_{ab}, which is initialize as \delta_{ab}. And since the eigenvalue are in order as -2 -1 0 1 2 ..., so I sum it in group.
				//
				real Lr = 0, Li = 0;
				int hKD = KD>>1;
				for (int k = 0; k < hKD; k++) {   // dim from [L+1,2]
					real angle = At *p[j] * lam[Il(apm,k)];
					real tmp1 = R[IR(apm,k,la)] * R[IR(apm,k,lb)], tmp2 = R[IR(apm,KD-k-1,la)] * R[IR(apm,KD-k-1,lb)];
					//real tmp1 = R[IR(apm,la,k)] * R[IR(apm,lb,k)], tmp2 = R[IR(apm,la,KD-k-1)] * R[IR(apm,lb,KD-k-1)];
					Lr += cos(angle) * ( tmp1 + tmp2 ) ;
					Li -= sin(angle) * ( tmp1 - tmp2 );
				}
				Lr += (KD&1)*(R[IR(apm,hKD, la)] * R[IR(apm,hKD,lb)]);
				//Lr += (KD&1)*(R[IR(apm,la,hKD)] * R[IR(apm,lb,hKD)]);
				// =========================>>>>

				int lpmj=IF(lb+apm,m+lb-la,j); // Note the index of m in the inner loop may differ from the outer one, even the physical m is the same.
				out_r[lmj] += Lr*in_r[lpmj] - Li*in_i[lpmj];
				out_i[lmj] += Lr*in_i[lpmj] + Li*in_r[lpmj];
		}
		//__syncthreads();   No need here.
}


void pTDSE::gEvolveInt(real*_RESTRICT_ in_r, real*_RESTRICT_ in_i, real*_RESTRICT_ out_r, real*_RESTRICT_ out_i) {

	dim3 dimGrid(Lmax+1, 2*Lmax+1);  // l,m
	dim3 blkGrid(pDIM, 1);
	gEvolveIntKernel<<<dimGrid, blkGrid>>>(in_r, in_i, out_r, out_i, At, g_p, g_lam, g_R, pDIM);
    cudaDeviceSynchronize();
	
}


